# Julius_Fiedler

RL-Agent for High-Level Decision-Making

**This code is currently not executable, since core features are property of IAV and not publicly available!!**

0. Setup
   1. Install SUMO Version 1.9.0 (https://sumo.dlr.de/releases/1.9.0/)
   2. Add `SUMO-HOME` to your environment variables (https://sumo.dlr.de/docs/Basics/Basic_Computer_Skills.html#sumo_home)
   3. Create a new python environment and install the required packages from `requirements.txt`


1. basic Usage:

    1. Run Ontology Framework (This is IAV property and not publicly available)
        - checkout **for_RL**-Branch of Ontology Framework
        - navigate to the root folder of the ontology-project and run `python main.py --pub_port x --sub_port y` 
        from the command line
    2. Select the Mode of the RL-Framework
        - Open `framework/runner.py` in an editor of your choice and select the Agent-Mode by commenting out the respective lines in `run_simulation_loop()`. Implemented modes are
          - Training
          - Evaluation
          - Debug mode / manual mode
    3. Run the Program
        - Run `python framework/main.py --pub_port y --sub_port x` from the command line in the root folder of the RL-project
        - Beware of the fact that the publisher port given to the onology has to be the subscriber port of the RL-project and vice versa
    4. The simulation window should open and the simulation should start


2. Customization:
    - `--render False` can be passed as an argument when running the program in order to prevent the simulation from rendering
    - `--seed x` can be passed as an argument to specify a seed for random number generation
    - Different simulation types can be selected in `framework/main.py`
    - Parameters files for the reward function are located in `framework/gym_sumo/` and titled simulation_type`_constants.py`                
3. Troubleshooting
   - If the setup of the environtment variable for SUMO was unsuccessful, try adding it to the system variables instead of the used variables
   - Check if the ontology command line window stated "OpenDrive Map loaded!".
   - Varify that the port numbers are used as stated above. Varify that these ports are not used by a different application.
