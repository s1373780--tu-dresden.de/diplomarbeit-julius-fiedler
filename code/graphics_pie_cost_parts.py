import matplotlib.pyplot as plt
import numpy as np
import os
import csv
import pandas as pd


"""compare the shares of the cost function"""



parentpath = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__))))

plt.rcParams.update({'font.size': 14})
# plt.rcParams['text.usetex'] = True

titles = [0,0,0,0]
folder_names = [0,0,0,0]
file_names = [0,0,0,0]
labels = [0,0,0,0]
# Comparison 1: normal Training
titles = "Training in Konfiguration 1 \nEvaluation Konfiguration 1"
folder_names = ["2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar",
               "2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar",
              #  "2022_01_31__13_03_00__sbl3_PPO_Model_three_lane_highway_multicar",
                "if_else_agent"]

file_names = ["eval_data_[1, 0, 0]_seed_1.csv", 
                "eval_data_[1, 0, 0]_seed_1.csv",
                "eval_data_[1, 0, 0]_seed_1.csv"]

labels = ["uninformierter Agent",
            "informierter Agent",
            "regelbasierter Agent"]

# Comparison 2: all 3 in difficult scenarios
# title = "Training in Konfiguration 1 \nEvaluation Konfiguration 2"
# folder_names = ["2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar",
#                "2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar",
#                 "if_else_agent"]

# file_names = ["eval_data_[0.5, 0.25, 0.25]_seed_1.csv", 
#                 "eval_data_[0.5, 0.25, 0.25]_seed_1.csv",
#                 "eval_data_[0.5, 0.25, 0.25]_seed_1.csv"]

# labels = ["uninformierter Agent",
#             "informierter Agent",
#             "regelbasierter Agent"]

## Comparison 3: advantage of difficult scenarios during training 
# title = "informierter Agent"
# folder_names = ["2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar",
#                "2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar",
#                 "2022_01_31__13_03_00__sbl3_PPO_Model_three_lane_highway_multicar",
#                 "2022_01_31__13_03_00__sbl3_PPO_Model_three_lane_highway_multicar"]

# file_names = ["eval_data_[1, 0, 0]_seed_1.csv", 
#                 "eval_data_[0.5, 0.25, 0.25]_seed_1.csv",
#                 "eval_data_[1, 0, 0]_seed_1.csv", 
#                 "eval_data_[0.5, 0.25, 0.25]_seed_1.csv"]

# labels = ["Training in Konfiguration 1 \nEvaluation in Konfiguration 1",
#             "Training in Konfiguration 1 \nEvaluation in Konfiguration 2",
#             "Training in Konfiguration 2 \nEvaluation in Konfiguration 1",
#             "Training in Konfiguration 2 \nEvaluation in Konfiguration 2"]

## Comparison 4: advantage of difficult scenarios during training 
# title = "uninformierter Agent"
# folder_names = ["2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar",
#                "2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar",
#                 "2022_01_27__09_56_49__sbl3_PPO_Model_three_lane_highway_multicar",
#                 "2022_01_27__09_56_49__sbl3_PPO_Model_three_lane_highway_multicar"]

# file_names = ["eval_data_[1, 0, 0]_seed_1.csv", 
#                 "eval_data_[0.5, 0.25, 0.25]_seed_1.csv",
#                 "eval_data_[1, 0, 0]_seed_1.csv", 
#                 "eval_data_[0.5, 0.25, 0.25]_seed_1.csv"]

# labels = ["Training in Konfiguration 1 \nEvaluation in Konfiguration 1",
#             "Training in Konfiguration 1 \nEvaluation in Konfiguration 2",
#             "Training in Konfiguration 2 \nEvaluation in Konfiguration 1",
#             "Training in Konfiguration 2 \nEvaluation in Konfiguration 2"]



## weights
w_collision = 10000
w_safetygap = 10
w_right_overtake = 20
w_speeding = 10
w_not_right = 3
w_state_change = 1   


assert len(folder_names) == len(file_names) == len(labels)
values = np.zeros(len(folder_names))
fig, ax = plt.subplots(2,2, figsize=(7,6))
for i in range(len(folder_names)):


    csv_path = os.path.join(parentpath, "framework//agent//models", folder_names[i], file_names[i])
    save_path = os.path.join(parentpath, "framework//agent//models", folder_names[i], "eval.txt")

    ### Eval Data Logging
    data = pd.read_csv(csv_path)
    no_steps = data["episode_length"].sum()
    no_collisions = data["no_collision"].sum() + data["no_offroad"].sum()
    no_speeding = data["no_speeding"].sum()
    no_safetygap_violation = data["no_safety_gap_violation"].sum()
    no_right_overtake = data["no_right_overtake"].sum()
    no_not_right = no_steps - data["keeping_right"].sum()
    no_state_change = data["no_state_changing_actions"].sum()
    actions_before_collision = np.zeros(5)
    for action in range(5):
        actions_before_collision[action] = len(data["action_before_collision"][data["action_before_collision"]==action])
    print(labels[i])
    print("actions before collision: ", actions_before_collision)

    value_coll = w_collision * no_collisions
    value_speeding = w_speeding * no_speeding
    value_safetygap = w_safetygap * no_safetygap_violation
    value_right_overtake = w_right_overtake * no_right_overtake
    value_not_right = w_not_right * no_not_right
    value_state_change = w_state_change * no_state_change

    value_summands = np.array([value_coll, value_right_overtake, value_safetygap, value_speeding,   \
        value_not_right, value_state_change])
    value_summands_labels = np.array(["Kosten f. Kollision "+r"$K_1$", "Kosten f. rechts\nüberholen "+r"$K_2$", \
        "Kosten f. Verletzung \nSicherheitsabstand "+r"$K_3$", "Kosten f. Geschwindig-\nkeitsüberschreitung "+r"$K_4$", \
        "Kosten f. Verletzung \nRechtsfahrgebot "+r"$K_5$", "Kosten f. Zustands-\nänderung "+r"$K_6$"])
    colors = np.array(["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple", "tab:brown"])
    ## mask summands of value 0 
    value_summands_labels = value_summands_labels[np.nonzero(value_summands)[0]]
    colors = colors[np.nonzero(value_summands)[0]]
    value_summands = value_summands[np.nonzero(value_summands)[0]]
    

    total = w_collision * no_collisions \
            + w_safetygap * no_safetygap_violation\
            + w_right_overtake * no_right_overtake\
            + w_speeding * no_speeding\
            + w_not_right * no_not_right\
            + w_state_change * no_state_change 
        
    # plt.rcParams["figure.figsize"] = (30,30)

    def make_autopct(values):
        def my_autopct(pct):
            if pct>1:
                return '{p:1.0f}% '.format(p=pct)
            else:
                return
        return my_autopct


    #autopct='%1.0f%%',
    ax[int(i>1),i%2].pie(value_summands/total,  autopct=make_autopct(value_summands), shadow=True, startangle=90, \
            pctdistance=0.7, colors=colors) 
    ax[int(i>1),i%2].set_title(labels[i])
    if i == 1:
        ax[0,1].legend(labels=value_summands_labels,  bbox_to_anchor=[1.3, 0])



    values[i] = total / no_steps

    
ax[1,1].axis("off")




plt.show()
