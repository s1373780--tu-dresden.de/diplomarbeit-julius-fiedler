import matplotlib.pyplot as plt
import numpy as np
import os
import csv
import pandas as pd

"""compare costs of different agents under different conditions"""


parentpath = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__))))

plt.rcParams.update({'font.size': 14})
# plt.rcParams['text.usetex'] = True

titles = [0,0,0,0]
folder_names = [0,0,0,0]
file_names = [0,0,0,0]
labels = [0,0,0,0]
# Comparison 1: normal Training
titles[0] = "Eval. in Konf. 1"
folder_names[0] = ["2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar",
               "2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar",
                "if_else_agent"]

file_names[0] = ["eval_data_[1, 0, 0]_seed_1.csv", 
                "eval_data_[1, 0, 0]_seed_1.csv",
                "eval_data_[1, 0, 0]_seed_1.csv"]

labels[0] = ["uninformierter Agent",
            "informierter Agent",
            "regelbasierter Agent"]

# Comparison 2: all 3 in difficult scenarios
titles[1] = "Eval. in Konf. 2"
folder_names[1] = ["2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar",
               "2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar",
                "if_else_agent"]

file_names[1] = ["eval_data_[0.5, 0.25, 0.25]_seed_1.csv", 
                "eval_data_[0.5, 0.25, 0.25]_seed_1.csv",
                "eval_data_[0.5, 0.25, 0.25]_seed_1.csv"]

labels[1] = ["uninformierter Agent",
            "informierter Agent",
            "regelbasierter Agent"]

## Comparison 3: advantage of difficult scenarios during training 
titles[2] = "informierter Agent"
folder_names[2] = ["2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar",
               "2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar",
                "2022_01_31__13_03_00__sbl3_PPO_Model_three_lane_highway_multicar",
                "2022_01_31__13_03_00__sbl3_PPO_Model_three_lane_highway_multicar"]

file_names[2] = ["eval_data_[1, 0, 0]_seed_1.csv", 
                "eval_data_[0.5, 0.25, 0.25]_seed_1.csv",
                "eval_data_[1, 0, 0]_seed_1.csv", 
                "eval_data_[0.5, 0.25, 0.25]_seed_1.csv"]

labels[2] = ["Training in Konfiguration 1 \nEvaluation in Konfiguration 1",
            "Training in Konfiguration 1 \nEvaluation in Konfiguration 2",
            "Training in Konfiguration 2 \nEvaluation in Konfiguration 1",
            "Training in Konfiguration 2 \nEvaluation in Konfiguration 2"]

## Comparison 4: advantage of difficult scenarios during training 
titles[3] = "uninformierter Agent"
folder_names[3] = ["2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar",
               "2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar",
                "2022_01_27__09_56_49__sbl3_PPO_Model_three_lane_highway_multicar",
                "2022_01_27__09_56_49__sbl3_PPO_Model_three_lane_highway_multicar"]

file_names[3] = ["eval_data_[1, 0, 0]_seed_1.csv", 
                "eval_data_[0.5, 0.25, 0.25]_seed_1.csv",
                "eval_data_[1, 0, 0]_seed_1.csv", 
                "eval_data_[0.5, 0.25, 0.25]_seed_1.csv"]

labels[3] = ["Training in Konfiguration 1 \nEvaluation in Konfiguration 1",
            "Training in Konfiguration 1 \nEvaluation in Konfiguration 2",
            "Training in Konfiguration 2 \nEvaluation in Konfiguration 1",
            "Training in Konfiguration 2 \nEvaluation in Konfiguration 2"]



## weights
w_collision = 10000
w_safetygap = 10
w_right_overtake = 20
w_speeding = 10
w_not_right = 3
w_state_change = 1   




values = [np.zeros(len(folder_names[0])), np.zeros(len(folder_names[1])), \
        np.zeros(len(folder_names[2])), np.zeros(len(folder_names[3]))]
fig, ax = plt.subplots(2,2)

for k in range(4):
    for i in range(len(folder_names[k])):


        csv_path = os.path.join(parentpath, "framework//agent//models", folder_names[k][i], file_names[k][i])
        save_path = os.path.join(parentpath, "framework//agent//models", folder_names[k][i], "eval.txt")

        ### Eval Data Logging
        data = pd.read_csv(csv_path)
        no_steps = data["episode_length"].sum()
        no_collisions = data["no_collision"].sum() + data["no_offroad"].sum()
        no_speeding = data["no_speeding"].sum()
        no_safetygap_violation = data["no_safety_gap_violation"].sum()
        no_right_overtake = data["no_right_overtake"].sum()
        no_not_right = no_steps - data["keeping_right"].sum()
        no_state_change = data["no_state_changing_actions"].sum()
        actions_before_collision = np.zeros(5)
        for action in range(5):
            actions_before_collision[action] = len(data["action_before_collision"][data["action_before_collision"]==action])
        print(labels[k][i])
        print("actions before collision: ", actions_before_collision)

        value_coll = w_collision * no_collisions
        value_speeding = w_speeding * no_speeding
        value_safetygap = w_safetygap * no_safetygap_violation
        value_right_overtake = w_right_overtake * no_right_overtake
        value_not_right = w_not_right * no_not_right
        value_state_change = w_state_change * no_state_change

        value_summands = np.array([value_coll, value_right_overtake, value_safetygap, value_speeding,   \
            value_not_right, value_state_change])
        value_summands_labels = np.array(["Kollision "+r"$K_1$", "Überholverbot\nrechts "+r"$K_2$", \
            "Verletzung \nSicherheitsabstand "+r"$K_3$", "Geschwindigkeits-\nüberschreitung "+r"$K_4$", \
            "Rechtsfahrgebot "+r"$K_5$", "Zustandsänderung "+r"$K_6$"])
        colors = np.array(["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple", "tab:brown"])
        ## mask summands of value 0 
        value_summands_labels = value_summands_labels[np.nonzero(value_summands)[0]]
        colors = colors[np.nonzero(value_summands)[0]]
        value_summands = value_summands[np.nonzero(value_summands)[0]]
        

        total = w_collision * no_collisions \
                + w_safetygap * no_safetygap_violation\
                + w_right_overtake * no_right_overtake\
                + w_speeding * no_speeding\
                + w_not_right * no_not_right\
                + w_state_change * no_state_change 
            
        # plt.rcParams["figure.figsize"] = (16,12)
        
        # ax[int(i>1),i%2].pie(value_summands/total,  autopct='%1.0f%%', shadow=True, startangle=90,\
        #     pctdistance=0.8, colors=colors) 
        # ax[int(i>1),i%2].set_title(labels[i])
        # if i == 1:
        #     ax[0,1].legend(labels=value_summands_labels,  bbox_to_anchor=[1.5, 0])
        # ax[1,1].axis("off")


        values[k][i] = total / no_steps

        # print("value of run: ", values[i])



    
# plt.show()
# plt.plot(values, label=labels)
plt.rcParams["figure.figsize"] = (8,6)
plt.rcParams.update({'figure.autolayout': True})
bar_height = 0.35
colors = ["r", "g", "tab:blue", "tab:orange"]

for i in range(2):
    fig1, ax1 = plt.subplots()
    index = np.arange(len(folder_names[2*i]))
    ax1.barh(y=index, width=values[2*i], height=bar_height, label=titles[2*i], color=colors[2*i])#, tick_label=labels)
    ax1.barh(y=index + bar_height, width=values[2*i+1], height=bar_height, label=titles[2*i+1], color=colors[2*i+1])#, tick_label=labels)
    ax1.set_yticklabels(labels[2*i])
    ax1.set_yticks(index + bar_height / 2)
    ax1.set_xlim(0,10)
    if i==1:
        ax1.set_ylim(-1,3.8)
    ax1.legend()
    ax1.invert_yaxis()
    # ax1.set_title(title)
    ax1.set_xlabel(r"verallgemeinerte Kosten $F$ pro Meter")
    plt.show()

