import matplotlib.pyplot as plt
import numpy as np
import os
import csv
import pandas as pd

"""visualize the logged training data"""



parentpath = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__))))
plt.rcParams.update({'font.size': 16})
# folder_name = "2022_01_22__21_18_28__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2022_01_17__21_23_15__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2022_01_23__17_02_51__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2021_12_26__23_59_15__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2021_12_27__00_00_51__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2022_01_05__14_29_39__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2022_01_09__23_33_54__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2022_01_09__23_34_55__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2022_01_11__12_44_29__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2022_01_11__15_26_31__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2022_01_13__10_19_57__sbl3_PPO_Model_three_lane_highway_multicar"
# folder_name = "2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar"
folder_name = "2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar"

tmp_path = os.path.join(parentpath, "framework//agent//tensorboard_logs", folder_name, "tmp")
save_path = os.path.join(parentpath, "framework//agent//models", folder_name)

### Neural Net Weight Logging
# weight_log_path = os.path.join(tmp_path, "weight_updates.csv")
# weight_data = pd.read_csv(weight_log_path)
# weight_data.plot(x="Step", y=["Action Net Change vs Initial", "Value Net Change vs Initial"])
# weight_data.plot(x="Step", y=["Action Net Change vs Last", "Value Net Change vs Last"], logx=True)


### Training Data Logging
training_log_path = os.path.join(tmp_path, "training_data.csv")
training_data = pd.read_csv(training_log_path)
training_data = training_data.drop([0])
training_data["Episode"] = training_data["Episode"]-training_data["Episode"][1]
rolling = training_data.rolling(window=10,min_periods=1)
titles1 = ["Episodenlänge", "Ertrag", "durchschn. Belohnung pro Schritt",  "durchschn. Geschwindigkeit [m/s]"]
fig, ax = plt.subplots(2, 2, sharex="col", figsize=[11,8])
ax[0,0].plot(rolling["episode_length"].mean())
ax[0,0].set_title(titles1[0])

ax[0,1].plot(rolling["total_reward_compare"].mean())
ax[0,1].set_title(titles1[1])

ax[1,0].plot((training_data["total_reward_compare"]/ training_data["episode_length"]).rolling(window=10,min_periods=1).mean())
ax[1,0].set_title(titles1[2])
ax[1,0].set_xlabel("Episode")


ax[1,1].plot(training_data["Episode"], (training_data["total_velocity"]/ training_data["episode_length"]).rolling(window=10,min_periods=1).mean())
ax[1,1].set_title(titles1[3])
ax[1,1].set_xlabel("Episode")
ax[1,1].set_ylim(0,20)

fig.tight_layout()
fig.savefig(os.path.join(save_path,"log1.pdf"))


titles2 = ["Anteil Kollisionen [%]", "Anteil Regelverstöße [%]", "Spurverteilung [%]", "Anteil ungünstiger Aktionen [%]"]
labels2 = ["Kollisionen", "Verlassen der Straße", "Geschw. Überschr.",\
            "Rechts überholen", "Verletzung Sicherheitsabstand", "links", \
            "mittig", "rechts"]
fig2, ax2 = plt.subplots(2, 2, sharex="col", figsize=[11,8])
# ax2[0,0].plot(rolling["no_collision"].mean(), label=labels2[0])
# ax2[0,0].plot(rolling["no_offroad"].mean(), label=labels2[1])
ax2[0,0].plot((training_data["no_collision"]/ training_data["episode_length"]*100).rolling(window=10,min_periods=1).mean(), label=labels2[0])
ax2[0,0].plot((training_data["no_offroad"]/ training_data["episode_length"]*100).rolling(window=10,min_periods=1).mean(), label=labels2[1])
ax2[0,0].set_title(titles2[0])
ax2[0,0].legend()

# ax2[0,1].plot(rolling["no_speeding"].mean(), label=labels2[2])
# ax2[0,1].plot(rolling["no_right_overtake"].mean(), label=labels2[3])
# ax2[0,1].plot(rolling["no_safety_gap_violation"].mean(), label=labels2[4])
ax2[0,1].plot((training_data["no_speeding"]/ training_data["episode_length"]*100).rolling(window=10,min_periods=1).mean(), label=labels2[2])
ax2[0,1].plot((training_data["no_right_overtake"]/ training_data["episode_length"]*100).rolling(window=10,min_periods=1).mean(), label=labels2[3])
ax2[0,1].plot((training_data["no_safety_gap_violation"]/ training_data["episode_length"]*100).rolling(window=10,min_periods=1).mean(), label=labels2[4])

ax2[0,1].set_title(titles2[1])
ax2[0,1].legend()

ax2[1,0].plot(training_data["Episode"], (training_data["no_left"]/ training_data["episode_length"]*100).rolling(window=10,min_periods=1).mean(), label=labels2[5])
ax2[1,0].plot(training_data["Episode"], (training_data["no_middle"]/ training_data["episode_length"]*100).rolling(window=10,min_periods=1).mean(), label=labels2[6])
ax2[1,0].plot(training_data["Episode"], (training_data["no_right"]/ training_data["episode_length"]*100).rolling(window=10,min_periods=1).mean(), label=labels2[7])
ax2[1,0].set_title(titles2[2])
ax2[1,0].legend()
ax2[1,0].set_xlabel("Episode")
ax2[1,0].set_ylim(0,80)



ax2[1,1].plot((training_data["no_invalid_action"]/ training_data["episode_length"]*100).rolling(window=10,min_periods=1).mean())
ax2[1,1].set_title(titles2[3])
ax2[1,1].set_xlabel("Episode")


fig2.tight_layout()
fig2.savefig(os.path.join(save_path,"log2.pdf"))

plt.show()
