import matplotlib.pyplot as plt
import numpy as np
import os
import math
import csv

"""show what actions led to collisions"""


values_1 = np.array([2,0,23,2,1])
values_2 = np.array([0,32,8,0,1])
plt.rcParams.update({'font.size': 14})

index = np.arange(5)
bar_width = 0.35

fig, ax = plt.subplots()
summer = ax.bar(index,values_1, bar_width,
                label="informierter Agent")

winter = ax.bar(index+bar_width, values_2,
                 bar_width, label="uninformierter Agent")

ax.set_xlabel('Aktion')
ax.set_ylabel('Anzahl der Kollisionen')
ax.set_title('Aktionen, die zu Kollisionen führen')
ax.set_xticks(index + bar_width / 2)
ax.set_xticklabels(["rechts", "default", "links", "beschl.", "bremsen"])
ax.legend()

plt.show()