class MissingVehicleError(Exception):
    pass

class SimulationStuckError(Exception):
    pass

class MissingHelperClassError(Exception):
    pass

class ObservationError(Exception):
    pass

class ActionError(Exception):
    pass