import framework.gym_sumo.environment
import gym
import os
import sys
import numpy as np
import traci
import sumolib
import time
import msvcrt as m
import csv
import matplotlib.pyplot as plt
from collections import deque
import winsound

from framework.communication.client.zeromq_client import ZeroMQClient #TODO: IAV property
from framework.core.vehicle.ego_vehicle import EgoVehicle #TODO: IAV property
from framework.communication.data_handler.sumo.ref_sensor_data_handler import RefSensorDataHandler #TODO: IAV property
from framework.communication.data_handler.sumo.sens_obj_list_handler import SensObjListHandler #TODO: IAV property
from framework.core.framework_core import FrameworkCore #TODO: IAV property
from framework.core.params import SumoParameters #TODO: IAV property
import framework.communication.data_handler.sumo.com_constants as com_constants #TODO: IAV property

from framework.gym_sumo.gym_parameters import GymParameters 
from framework.agent.hyperparams import HyperParameters
from framework.agent.agent import Sbl3_Agent
import framework.tests.constants as const

os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE" # some rarely accuring error message requested this command


class Runner:

    def __init__(self, sumo_params: SumoParameters, gym_params: GymParameters, hyper_params: HyperParameters,\
                onto_publisher_port: int, onto_subscriber_port: int):
       
        # debug
        self.onto_time_stamp = 0
        self.step_possible_flag = True
        self.start_time = time.time()       
        self.onto_time_stamp_list = np.array([])

        # Parameters
        self.label = "con"
        self.sumo_params = sumo_params
        self.gym_params = gym_params
        self.hyper_params = hyper_params
        self.gym_params.steps_per_sec = self.sumo_params.steps_per_sec
        self.gym_params.pubs_per_sec = self.sumo_params.pubs_per_sec

        # Handler Setup #TODO: IAV property
        self.ego_vehicles = []
        self.ego_vehicles.append(EgoVehicle(self.gym_params.ego_vehicle_id, onto_publisher_port))
        self.framework = FrameworkCore(simulator='sumo',
                                    simulation_parameters=sumo_params)
        self.handlers = []
        self.handlers.append(RefSensorDataHandler())
        self.handlers.append(SensObjListHandler())
        

        # Communication Runner -> Ontology
        self.onto_publisher = ZeroMQClient() #TODO: IAV property
        self.onto_publisher.bind("tcp", "*", self.ego_vehicles[0].onto_publisher_port)
        self.onto_publisher.publish_threaded(self.ego_vehicles[0].onto_publisher_queue)

        # Simulation start
        traci.start([self._find_sumo(sumo_params.render), "-c", sumo_params.sumocfg, \
                    "--step-length", str(self.sumo_params.step_size), \
                    "--collision.action", "warn", "--collision.mingap-factor", "0", \
                    "--time-to-teleport", "-1"], label=self.label) #, "--lanechange.duration", "1"
        self.con = traci.getConnection(self.label)
        self.framework.pass_api(self.con) #TODO: IAV property
        
        # Open Gym
        self.env = gym.make("SumoGUI_Onto-v0", con=self.con, gym_params=gym_params)
        self.env.runner = self

        # add Agent
        self.agent = Sbl3_Agent(self.env, hyperparams=self.hyper_params)

        # start Handlers, contextSubscription
        for handler in self.handlers:
            handler.startup(sumo_params, self.ego_vehicles, None, self.framework)

        # Communication Ontology -> Runner
        self.onto_subscriber = ZeroMQClient() #TODO: IAV property
        self.onto_subscriber.connect(transport='tcp', host='127.0.0.1', port=onto_subscriber_port)
        self.onto_subscriber.subscribe([], callback=lambda topic, data : handle_message_onto(self, topic, data))
        self.ego_vehicles[0].on_road = True

    


    def _find_sumo(self, render):
        if render:
            return sumolib.checkBinary('sumo-gui')
        else:
            return sumolib.checkBinary('sumo')

    def close(self):
        self.onto_publisher.stop_client()
        self.onto_subscriber.stop_client()
        try: ##TODO this is a nasty way of closing the simulation
            self.con.simulation.close() 
            self.con.close()
        except Exception as exc:
            print("exception while closing SUMO: ", exc)
            
    def run_simulation_loop(self):
        """chose the agent mode and run the simulation"""
        if self.gym_params.simulation_type == const.TESTFELD_DD:
            time.sleep(2)
        exit_flag = False
        if self.agent is not None:
            self.env.reset()
            self.agent._check_env()
        
           
        
        print("running...")
        self.start_time = time.time()
        self.env.simulation_started = True  
        self.env.helper.random_starts = True
        obs = self.env.reset() 
        a = 3
        total_reward = 0
        self.last_actions = deque(np.array([1]), maxlen=5)


        while not (self.env.done or exit_flag):
            
            """Training"""

            # self.agent.train()
            # exit_flag = True
   


            """Evaluation
            chose an existing model to start evaluation run
            to load other models copy folder name 
            """

            ## if needed:
            # self.env.enable_debug_prints = True

            ## Loading Existing Models
            """uninformed Agent, trained with scenario-probabilities [1, 0, 0]"""
            # self.agent.load_model("2022_01_27__09_55_56__sbl3_PPO_Model_three_lane_highway_multicar\model.zip")
            # self.agent.play(30,False,False,False)
            # exit_flag = True
            """uninformed Agent, trained with scenario-probabilities [0.8, 0.1, 0.1]"""
            # self.agent.load_model("2022_01_27__09_56_49__sbl3_PPO_Model_three_lane_highway_multicar\model.zip")
            # self.agent.play(30,False,False,False)
            # exit_flag = True
            """informed Agent, trained with scenario-probabilities [1, 0, 0]"""
            self.agent.load_model("2022_01_27__13_47_38__sbl3_PPO_Model_three_lane_highway_multicar\model.zip")
            self.agent.play(30,False,False,False)
            exit_flag = True
            """informed Agent, trained with scenario-probabilities [0.5, 0.25, 0.25]"""
            # self.agent.load_model("2022_01_31__13_03_00__sbl3_PPO_Model_three_lane_highway_multicar\model.zip")
            # self.agent.play(30,False,False,False)
            # exit_flag = True
            """rulebased Agent"""
            # self.play(30)
            # exit_flag = True
            """load different model"""
            # folder_name = ...
            # self.agent.load_model(folder_name + "\model.zip")
            # self.agent.play(30,False,False,False) 
            # exit_flag = True



            """Debug Mode
            run in debug mode, set breakpoints, manually change "a" to the preferred action each step
            """
            
            # action = np.random.randint(0,5)
            # action = a

            # self.env.enable_debug_prints = True
            # obs, reawrd, terminal, terminalType = self.env.step(action)  
            # total_reward += reawrd
            # print("reward sum ", total_reward)
            # if terminal:
            #     self.env.reset()
            #     print("total reward: ", total_reward)
            #     exit_flag =True
           

        
        print("duration:", time.time()-self.start_time)
        
        print("closing")
        self.close()
        sys.stdout.flush()
        sys.exit()
    

    def request_update(self):
        """request update of sumo information, done by iav-handlers"""
        for handler in self.handlers:
            handler.handle_step_command(com_constants.SERIALIZE_AND_ENQUEUE)

    def get_deterministisc_action(self, obs):
        """rules for rule based agent"""
        if obs[self.env.helper.oc.LCR] and not 2 in self.last_actions:
            action = 0
        elif obs[self.env.helper.oc.ACC] and not self.last_actions[-1] == 4:
            action = 3
        elif obs[self.env.helper.oc.LCL] and obs[self.env.helper.oc.ABS_V]+1 < obs[self.env.helper.oc.MAX_SPEED]:
            action = 2
        elif obs[self.env.helper.oc.DEF]:
            action = 1
        elif obs[self.env.helper.oc.DEC]:
            action = 4
        else:
            action = -1
            print("deadlock!-------------------------------------------------")
            # winsound.Beep(1000, 1000)
            # m.getch()
        self.last_actions.append(action)  
        return action

    def play(self, num_episodes=5):
        """start rule based agent"""
        self.env.training = False
        self.env.helper.random_starts = False
        obs = self.env.reset()
        done = False
        total_reward = 0
        episodes = 0
        state = None
        obs_array = [np.array(obs[0:4])]
        action_array = np.array([])
        reward_array = np.array([])
        no_collisions = 0
        no_offroads = 0
        no_overtakes = 0
        step = 0
        no_traffic_rule_violations = 0
        no_deadlocks = 0
        no_lcr_possible_but_not_done = 0
        total_distance_traveled = 0
        print_heading = True
        while episodes < num_episodes:
            # get rule based action
            action = self.get_deterministisc_action(obs)
            if action == -1:
                done = True
                no_deadlocks += 1
            else:
                # take action and log some step data
                obs, reward, done, info = self.env.step(action)
                total_reward += reward
                step += 1
                if info["speed"] == "speeding" or info["safety gap violation"] or info["right overtake"]:
                    no_traffic_rule_violations += 1
                if info["successful overtake"] == 3:
                    no_overtakes += 1
                if info["collision"] or info["offroad"]:
                    no_collisions += 1
                if info["offroad"]:
                    no_offroads += 1
                if obs[self.env.helper.oc.IDX] > 0 and obs[self.env.helper.oc.LCR] and action!=0:
                    no_lcr_possible_but_not_done += 1


                reward_array = np.append(reward_array, total_reward)
                action_array = np.append(action_array, action)
                obs_array = np.append(obs_array, [obs[0:4]], axis=0)

            if done:      
                # log episode data        
                total_distance_traveled += self.env.con.vehicle.getDistance("1000")
                episodes += 1
                filename = "eval_data_" + str(self.env.helper.c.SCENARIO_PROBABILITIES) + "_seed_" + \
                    str(self.env.gym_params.seed) + ".csv"
                path = os.path.join(os.path.dirname(__file__), "agent//models//if_else_agent")
                self.env.helper.save_episode_data(path, print_heading, filename)
                print_heading = False
                obs = self.env.reset()
                print("total reward: ", total_reward)

        self.env.done = True
        print("done playing")

        # save step data
        v_total = 0
        lane_distribution_total = [0,0,0] #right middle left
        for i in range(len(obs_array)):
            v_total += obs_array[i][0]
            lane_distribution_total[int(obs_array[i][1])] += 1
        v_mean = v_total / step
        right = lane_distribution_total[0] / step * 100
        middle = lane_distribution_total[1] / step * 100
        left = lane_distribution_total[2] / step * 100
        lcr_possible_but_not_done_percent = no_lcr_possible_but_not_done / step * 100
        deadlock_rate = no_deadlocks / num_episodes * 100
        collision_rate = no_collisions / num_episodes * 100
        print("If-Else_Agent")
        print("Scenarios: ", self.env.helper.c.SCENARIO_PROBABILITIES)
        print("Number of Episodes: ", num_episodes)
        print("Total number of steps: ", step)
        print("Lane Distribution: Right: ", round(right,1), "%, Middle: ", round(middle,1), "%, Left: ", round(left,1), "%" )
        print("Number of Deadlocks: ", no_deadlocks)
        print("Number of Collisions: ", no_collisions)
        print("Of these collisions, this many were offroads: ", no_offroads)
        print("Number of Traffic Rule Violations: ", no_traffic_rule_violations, " (", round(no_traffic_rule_violations/step*100,1), "%)")
        print("Average Speed: ", round(v_mean, 1))
        print("Number of Overtakes: ", no_overtakes)
        print("Total Distance traveled: ", round(total_distance_traveled, 1), "m")
        print("Avg Distance between Collisions: ", round(total_distance_traveled / (no_collisions + 1), 1))
        print("Avg Distance between Deadlocks: ", round(total_distance_traveled / (no_deadlocks + 1), 1))
        print("Collision Rate: ", round(collision_rate, 1))
        print("Deadlock Rate: ", round(deadlock_rate, 1))
        
        script_dir = os.path.dirname(__file__)  # <-- absolute dir the script is in
        txt_path = os.path.join(script_dir,"agent\\models\\if_else_agent", "eval.txt")
        f = open(txt_path, "a")
        f.write("\nEvaluation:\n")
        f.write("".join(["\nScenarios: ", str(self.env.helper.c.SCENARIO_PROBABILITIES)]))
        f.write("".join(["\nSeed: ", str(self.env.gym_params.seed)]))
        f.write("".join(["\nNumber of Episodes: ", str(num_episodes)]))
        f.write("".join(["\nTotal number of steps: ", str(step)]))
        f.write("".join(["\nLane Distribution: Right: ", str(round(right,1)), "%, Middle: ", str(round(middle,1)), "%, Left: ", str(round(left,1)), "%" ]))
        f.write("".join(["\nNumber of Lane Change Right not done: ", str(no_lcr_possible_but_not_done), " (", str(round(lcr_possible_but_not_done_percent,1)), "%)"]))
        f.write("".join(["\nNumber of Deadlocks: ", str(no_deadlocks)])) 
        f.write("".join(["\nNumber of Collisions: ", str(no_collisions)]))
        f.write("".join(["\nOf these Col, some were Offroads: ", str(no_offroads)]))
        f.write("".join(["\nNumber of Traffic Rule Violations: ", str(no_traffic_rule_violations), " (", str(round(no_traffic_rule_violations/step*100,1)), "%)"]))
        f.write("".join(["\nAverage Speed: ", str(round(v_mean, 1))]))
        f.write("".join(["\nNumber of Overtakes: ", str(no_overtakes)]))
        f.write("".join(["\nTotal Distance traveled: ", str(round(total_distance_traveled, 1)), "m"]))
        f.write("".join(["\nAvg Distance between Collisions: ", str(round(total_distance_traveled / (no_collisions + 1), 1))]))
        f.write("".join(["\nAvg Distance between Deadlocks: ", str(round(total_distance_traveled / (no_deadlocks + 1), 1))]))
        f.write("".join(["\nDeadlock Rate: ", str(round(deadlock_rate, 1)), "%"]))
        f.write("".join(["\nCollision Rate: ", str(round(collision_rate, 1)), "%"]))
        f.close()

def handle_message_onto(runner : Runner, topic, data):
    """receive data packages from ontology and send them to environment"""
    rcv = np.copy(np.frombuffer(data, dtype="float64"))
    if topic == com_constants.VEHICLE_NAMES:
        time_stamp = rcv[0]
        close_vehicles = np.array(np.array(rcv[1:10], dtype=int), dtype=str)
        runner.env.update_coloring(time_stamp, close_vehicles)
	
    if topic == com_constants.OBSERVATIONS:
        runner.onto_time_stamp = rcv[0]
        delta_x = rcv[1:10]
        delta_y = rcv[10:19]
        rel_v = rcv[19:28] 
        lane = rcv[28:37]
        occupation = rcv[37:46]
        runner.env.update_observations(runner.onto_time_stamp, delta_x, delta_y, rel_v, lane, occupation)

    if topic == com_constants.CURRENT_LANE_IDX:
        time_stamp = rcv[0]
        sumo_idx = int(rcv[1])
        drivable_lanes = int(rcv[2])
        runner.env.update_current_lane_index(time_stamp, sumo_idx, drivable_lanes)

    if topic == com_constants.OVERTAKE_STATE:
        time_stamp = rcv[0]
        ov_state = rcv[1]
        runner.env.update_overtake_state(time_stamp, ov_state)

    if topic == com_constants.ACTION_EVALUATION:
        time_stamp = rcv[0]
        possible_actions = rcv[1:6]
        runner.env.update_possible_actions(time_stamp, possible_actions)
    
    if topic == com_constants.SPEED:
        time_stamp = rcv[0]
        max_speed = rcv[1] 
        ego_speed = rcv[2] 
        runner.env.update_speed(time_stamp, max_speed, ego_speed)