import os
import sys
parentpath = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)),os.pardir))
print(parentpath)
sys.path.insert(1, parentpath)
import argparse
import numpy as np

from framework.core.params import SumoParameters

from framework.runner import Runner
from framework.gym_sumo.gym_parameters import GymParameters
from framework.agent.hyperparams import HyperParameters
import framework.tests.constants as const

def parse_arguments():
    argparser = argparse.ArgumentParser(description=__doc__)

    argparser.add_argument('--pub_port', type=int, default=5564,
                           help='TCP publisher port for connection to ontology (default: 5564)')
    argparser.add_argument('--sub_port', type=int, default=5560,
                           help='TCP subscriber port for connection to ontology (default: 5560)')
    argparser.add_argument('--render', type=bool, default=True,
                           help='show simulation? (default: True)')
    argparser.add_argument('--seed', type=int, default=1,
                           help='seed for training and vehicle spawning (default: 1)')

    return argparser.parse_args()

    

def set_sumo_params(global_sumocfg, context_range):
    """ Set all sumo parameters
    """
    return SumoParameters(sumocfg=global_sumocfg,
                          render=render,
                          real_time=False,
                          multiprocessing=False,
                          steps_per_sec=10, pubs_per_sec=10,
                          pubs_per_sec_v2x=1,
                          context_range=context_range, v2x_context_range=context_range,
                          v2x_sensor_range=context_range,
                          lane_change_mode=int(0),
                          speed_mode=int(0),
                          dashboard_control=False)

def set_gym_params(ego_vehicle_id, simulation_type, max_speed, context_range, number_of_actions, seed):
    """ Set all gym parameters
    """
    return GymParameters(ego_vehicle_id, 
                    simulation_type = simulation_type,
                    mode="gui", 
                    max_speed=max_speed, 
                    context_range=context_range,
                    number_of_actions=number_of_actions,
                    seed=seed)

def set_hyper_params(): 
    return HyperParameters()


### Parameters ### 
## General Parameters ##
# simulation_type = const.THREE_LANE_CIRCLE
# simulation_type = const.TESTFELD_DD
# simulation_type = const.SCENARIO_2
# simulation_type = const.NARROW
# simulation_type = const.THREE_LANE_HIGHWAY
simulation_type = const.THREE_LANE_HIGHWAY_MULTICAR
# simulation_type = const.THREE_LANE_HIGHWAY_MULTICAR_DETERMINISTIC

ego_vehicle_id = "1000"

render = True
context_range = 100
max_speed = 30 # m/s

number_of_actions = 5

## Simulation Specific Parameters ## 
if simulation_type == const.THREE_LANE_CIRCLE:
    local_sumocfg = "framework/tests/sumo_configuration/three_lane_circle/config.sumocfg"

elif simulation_type == const.TESTFELD_DD:
    local_sumocfg = "framework/tests/sumo_configuration/testfeld_dd/config.sumocfg"

elif simulation_type == const.SCENARIO_1:
    local_sumocfg = "framework/tests/sumo_configuration/scenario_1/config.sumocfg"

elif simulation_type == const.SCENARIO_2:
    local_sumocfg = "framework/tests/sumo_configuration/scenario_1/config.sumocfg"

elif simulation_type == const.NARROW:
    local_sumocfg = "framework/tests/sumo_configuration/narrow_open_drive/config.sumocfg"

elif simulation_type == const.THREE_LANE_HIGHWAY:
    local_sumocfg = "framework/tests/sumo_configuration/three_lane_highway/config.sumocfg"

elif simulation_type == const.THREE_LANE_HIGHWAY_MULTICAR:
    local_sumocfg = "framework/tests/sumo_configuration/three_lane_highway_multicar/config.sumocfg"

elif simulation_type == const.THREE_LANE_HIGHWAY_MULTICAR_DETERMINISTIC:
    local_sumocfg = "framework/tests/sumo_configuration/three_lane_highway_multicar/config.sumocfg"


global_sumocfg = os.path.join(parentpath, local_sumocfg)

###################################################################################################
def main():
    args = parse_arguments()
    sumo_params = set_sumo_params(global_sumocfg, context_range)
    gym_params = set_gym_params(ego_vehicle_id, simulation_type, max_speed, context_range, number_of_actions, seed=args.seed)
    hyper_params = set_hyper_params()
    runner = Runner(sumo_params, gym_params, hyper_params, args.pub_port, args.sub_port)

    runner.run_simulation_loop()


if __name__ == "__main__":
    main()

