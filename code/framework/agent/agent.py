import stable_baselines3
import gym
import msvcrt as m
import os
import sys
import datetime as dt
import numpy as np
import time
import datetime
import matplotlib.pyplot as plt
import csv
import winsound

from stable_baselines3 import PPO
from stable_baselines3 import DQN
from stable_baselines3 import A2C
from stable_baselines3.common import results_plotter
from stable_baselines3.common.results_plotter import load_results, ts2xy, plot_results
from stable_baselines3.common.env_checker import check_env
from stable_baselines3.common.evaluation import evaluate_policy
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.vec_env import DummyVecEnv
from framework.agent.callbacks import SaveOnBestTrainingRewardCallback



from framework.agent.hyperparams import HyperParameters

script_dir = os.path.dirname(__file__)  # <-- absolute dir the script is in
rel_path = "models"

models_path = os.path.join(script_dir, rel_path)


class Sbl3_Agent:

    def __init__(self, env: gym.Env, hyperparams: HyperParameters) -> None:
        self.env = env
        
        self.hyperprams = hyperparams
        self.policy = hyperparams.policy
        self.total_timesteps = hyperparams.total_timesteps
        self.verbose = hyperparams.verbose
        self.batch_size = hyperparams.batch_size
        self.n_eval_episodes = hyperparams.n_eval_episodes
        self.gamma = hyperparams.gamma
        self.play_time = hyperparams.play_time
        self.agent_type = hyperparams.agent_type
        self.learning_rate = hyperparams.learning_rate
        self.n_epochs = hyperparams.n_epochs

        t = dt.datetime.now().strftime("%Y_%m_%d__%H_%M_%S_")
        self.model_name = "_".join([t, "sbl3", self.agent_type, "Model", self.env.gym_params.simulation_type])
        

    def create_model(self, agent_type):
        """generate the appropriate paths and folders, wrappers and model parameters"""
        self.model_path = os.path.join(models_path, self.model_name)
        try:
            os.makedirs(self.model_path)
        except:
            pass
        
        self.log_path = os.path.join(script_dir, "tensorboard_logs", self.model_name)
        self.log_dir = os.path.join(self.log_path, "tmp/")
        os.makedirs(self.log_dir, exist_ok=True)
        self.env = Monitor(self.env, self.log_dir, allow_early_resets=True)
        if agent_type == "PPO":
            self.model = PPO(self.policy, self.env, verbose=self.verbose,  batch_size=self.batch_size,
                    learning_rate=self.learning_rate, n_epochs=self.n_epochs, gamma=self.gamma, \
                    ent_coef=self.hyperprams.ent_coef, tensorboard_log= self.log_path, seed=self.env.gym_params.seed)
        elif agent_type == "A2C":
            self.model = A2C(self.policy, self.env, verbose=self.verbose,
                    learning_rate=self.learning_rate, gamma=self.gamma, tensorboard_log= self.log_path,\
                    seed=self.env.gym_params.seed)
        elif self.agent_type == "DQN":
            self.model = DQN(self.policy, self.env, verbose=self.verbose,  batch_size=self.batch_size,
                    learning_rate=self.learning_rate, gamma=self.gamma, \
                    exploration_fraction=self.hyperprams.exploration_fraction,\
                    exploration_initial_eps=self.hyperprams.exploration_initial_eps, \
                    exploration_final_eps=self.hyperprams.exploration_final_eps, tensorboard_log= self.log_path,\
                    seed=self.env.gym_params.seed)         

    def _check_env(self):
        check_env(self.env)
        print("environment check complete")

    def train(self):
        """train the model"""
        self.env.training = True
        self.create_model(self.agent_type)
        self.save_hyperparams()

        start_time = time.time()
        print("Start of training...")
        # callback function for logging during training
        callback = SaveOnBestTrainingRewardCallback(check_freq=1000, log_dir=self.log_dir, save_path=self.model_path)
        self.model.learn(
            total_timesteps=self.total_timesteps, 
            callback=callback
        )
        training_time = time.time() - start_time
        print(f"Training finished after {str(datetime.timedelta(seconds=training_time))} seconds.")

        self.save_model()
        # evaluation episodes
        self.play(30, False, True, False)

    def retrain(self, filename: str):
        """load existing trained model and continue training"""
        path = os.path.join(models_path, filename)
        old_model = PPO.load(path)

        self.env.training = True
        self.create_model(self.agent_type)
        self.save_hyperparams()
        self.model.policy.load_from_vector(old_model.policy.parameters_to_vector())

        start_time = time.time()
        print("Start of training...")
        callback = SaveOnBestTrainingRewardCallback(check_freq=1000, log_dir=self.log_dir, \
                save_path=self.model_path, first_stop=self.env.helper.first_stop, agent_type=self.hyperprams.agent_type)
        self.model.learn(
            total_timesteps=self.total_timesteps, 
            callback=callback
        )
        training_time = time.time() - start_time
        print(f"Training finished after {str(datetime.timedelta(seconds=training_time))} seconds.")

        self.save_model()
        self.play(5, False, True, True)

    def save_hyperparams(self):    
        """save hyper parameter of training"""
        txt_path = os.path.join(self.model_path, "hyperparameters.txt")
        f = open(txt_path, "w")
        f.write(self.hyperprams.get_hyperparams())

        try:
            name = "_".join([str(self.env.gym_params.simulation_type), "constants.py"])
            rew_file = os.path.abspath(os.path.join(script_dir, os.pardir,"gym_sumo", name))
            rf = open(rew_file, "r")
            for line in rf:
                f.write(line)
        except:
            print("reward file not saved.")
        f.close()
    
    def save_model(self):
        filename = "model"
        path = os.path.join(self.model_path, filename)
        self.model.save(path)
        txt_path = os.path.join(self.model_path, "hyperparameters.txt")
        f = open(txt_path, "a")
        f.write("".join(["\nTotal Training Steps: ", str(self.env.sumo_step)]))
        f.write("".join(["\nSeed: ", str(self.env.gym_params.seed)]))
        f.close()

        print("model saved as ", path)

    def load_model(self, filename: str):
        self.agent_type = filename.split("sbl3_")[1].split("_Model")[0]        
        path = os.path.join(models_path, filename)
        self.model_name = filename.split("\\")[0]
        if self.agent_type == "PPO":
            self.model = PPO.load(path)
        elif self.agent_type == "A2C":
            self.model = A2C.load(path)
        elif self.agent_type == "DQN":
            self.model = DQN.load(path)
        else:
            raise NameError("Agent Type not supportet!")
        self.model_path = os.path.join(models_path, self.model_name)

    def play(self, num_episodes=5, show_plot=True, save_plot=False, save_to_csv=True):
        """play evalutaion episodes, log the data"""
        try:
            if self.env.class_name() == "Monitor":
                self.env.env.training = False
        except:
            self.env.training = False
        self.env.helper.random_starts = False
        obs = self.env.reset()
        done = False
        total_reward = 0
        episodes = 0
        state = None
        obs_array = [np.array(obs[0:4])]
        action_array = np.array([])
        reward_array = np.array([])
        no_collisions = 0
        no_offroads = 0
        no_overtakes = 0
        step = 0
        no_traffic_rule_violations = 0
        no_invalid_actions = 0
        no_lcr_possible_but_not_done = 0
        total_distance_traveled = 0
        print_heading = True
        while episodes < num_episodes:
            # use the trained model to chose the actions
            action, state = self.model.predict(obs, state=state, deterministic=True)
            obs, reward, done, info = self.env.step(action)
            total_reward += reward
            step += 1
            if info["speed"] == "speeding" or info["safety gap violation"] or info["right overtake"]:
                no_traffic_rule_violations += 1
            if info["successful overtake"] == 3:
                no_overtakes += 1
            if info["collision"] or info["offroad"]:
                no_collisions += 1
            if info["offroad"]:
                no_offroads += 1
            if info["invalid action"]:
                no_invalid_actions += 1
            if obs[self.env.helper.oc.IDX] > 0 and obs[self.env.helper.oc.LCR] and action != 0:
                no_lcr_possible_but_not_done += 1


            reward_array = np.append(reward_array, total_reward)
            action_array = np.append(action_array, action)
            obs_array = np.append(obs_array, [obs[0:4]], axis=0)

            if done:
                # log episode data
                total_distance_traveled += self.env.con.vehicle.getDistance("1000")
                episodes += 1
                filename = "eval_data_" + str(self.env.helper.c.SCENARIO_PROBABILITIES) + "_seed_" + \
                    str(self.env.gym_params.seed) + ".csv"
                self.env.helper.save_episode_data(self.model_path, print_heading, filename)
                print_heading = False
                obs = self.env.reset()
                print("total reward: ", total_reward)

        
        print("done playing")

        # safe data
        v_total = 0
        lane_distribution_total = [0,0,0] #right middle left
        for i in range(len(obs_array)):
            v_total += obs_array[i][0]
            lane_distribution_total[int(obs_array[i][1])] += 1
        v_mean = v_total / step
        right = lane_distribution_total[0] / step * 100
        middle = lane_distribution_total[1] / step * 100
        left = lane_distribution_total[2] / step * 100
        invalid_action_percent = no_invalid_actions / step * 100
        lcr_possible_but_not_done_percent = no_lcr_possible_but_not_done / step * 100
        collision_rate = no_collisions / num_episodes * 100
        print(self.model_name)
        print("Number of Episodes: ", num_episodes)
        print("Total number of steps: ", step)
        print("Lane Distribution: Right: ", round(right,1), "%, Middle: ", round(middle,1), "%, Left: ", round(left,1), "%" )
        print("Number of Lane Changes Right not done:", no_lcr_possible_but_not_done, " (", lcr_possible_but_not_done_percent,  "%)" )
        print("Number of Collisions: ", no_collisions)
        print("Of these collisions, this many were offroads: ", no_offroads)
        print("Number of Traffic Rule Violations: ", no_traffic_rule_violations, " (", no_traffic_rule_violations/step*100, "%)")
        print("Number of invalid Actions: ", no_invalid_actions, " (", invalid_action_percent, "%)")
        print("Average Speed: ", round(v_mean, 1))
        print("Number of Overtakes: ", no_overtakes)
        print("Total Distance traveled: ", round(total_distance_traveled, 1), "m")
        print("Avg Distance between Collisions: ", round(total_distance_traveled / (no_collisions + 1), 1))
        print("Collision Rate: ", round(collision_rate, 1))
       
        txt_path = os.path.join(self.model_path, "eval.txt")
        f = open(txt_path, "a")
        f.write("\nEvaluation\n")
        try:
            f.write("".join(["\nScenarios: ", str(self.env.helper.c.SCENARIO_PROBABILITIES)]))
        except:
            print("no scenario probabilities found")
        f.write("".join(["\nSeed: ", str(self.env.gym_params.seed)]))
        f.write("".join(["\nNumber of Episodes: ", str(num_episodes)]))
        f.write("".join(["\nTotal number of steps: ", str(step)]))
        f.write("".join(["\nLane Distribution: Right: ", str(round(right,1)), "%, Middle: ", str(round(middle,1)), "%, Left: ", str(round(left,1)), "%" ]))
        f.write("".join(["\nNumber of Lane Change Right not done: ", str(no_lcr_possible_but_not_done), " (", str(round(lcr_possible_but_not_done_percent,1)), "%)"]))
        f.write("".join(["\nNumber of Collisions: ", str(no_collisions)]))
        f.write("".join(["\nOf these Col, some were Offroads: ", str(no_offroads)]))
        f.write("".join(["\nNumber of Traffic Rule Violations: ", str(no_traffic_rule_violations), " (", str(round(no_traffic_rule_violations/step*100,1)), "%)"]))
        f.write("".join(["\nNumber of invalid Actions: ", str(no_invalid_actions), " (", str(round(invalid_action_percent,1)), "%)"]))
        f.write("".join(["\nAverage Speed: ", str(round(v_mean, 1))]))
        f.write("".join(["\nNumber of Overtakes: ", str(no_overtakes)]))
        f.write("".join(["\nTotal Distance traveled: ", str(round(total_distance_traveled, 1)), "m"]))
        f.write("".join(["\nAvg Distance between Collisions: ", str(round(total_distance_traveled / (no_collisions + 1), 1))]))
        f.write("".join(["\nCollision Rate: ", str(round(collision_rate, 1)), "%"]))
        f.close()


        if save_to_csv:
            self.save_data_to_csv([self.model_name], ["reward"], [reward_array])
        self.plot_episode(obs_array, action_array, reward_array, show_plot, save_plot)
    
    def save_data_to_csv(self, identifier, tag, data):
        """save some data array as csv for later comparison

        Args:
            identifier (array): unique name e.g. time stamp
            tag (array): description of data e.g. rewards
            data (array): data array
        """
        #TODO this is not currently used
        assert len(identifier) == len(tag) and len(identifier) == len(data)
        with open('data.csv', 'a+', newline='') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=' ',quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for i in range(len(identifier)):
                spamwriter.writerow([str(identifier[i])] + [str(tag[i])] + [str(data[i])])

    def plot_episode(self, obs_array, action_array, reward_array, show_plot, save_plot):
        """plot simple episode data"""
        fig, axs = plt.subplots(figsize=(16,9))
        fig.subplots_adjust(right=0.75)
        twin1 = axs.twinx()
        p1,p2,p3,p4 = axs.plot(obs_array, label=["speed", "lane idx", "#lanes", "speed limit"])
        p5 = twin1.scatter(np.arange(0,len(action_array),1), action_array+0.5, c="r", label="action", s=2)
        axs.set_ylim(-0.2, np.max(obs_array)+0.2)
        twin1.set_ylim(-0.2, np.max(obs_array)+0.2)
        axs.set_xlabel("Step")
        axs.set_ylabel("Value")
        twin1.set_ylabel("Action")
        twin1.yaxis.label.set_color("r")
        twin1.tick_params(axis='y', colors="r")
        names = ('LC Right', 'Default', 'LC Left', 'Accel', 'Decel')
        y_pos = np.arange(len(names))+0.5
        plt.yticks(y_pos, names)
        axs.legend(handles=[p1, p2, p3, p4, p5])
        plt.title("Actions and Observations in one Episode, total reward: "+ str(reward_array[-1]))

        if save_plot:
            abs_file_path = os.path.join(models_path, self.model_name, "plot.png")
            plt.savefig(abs_file_path, bbox_inches="tight")
        if show_plot:
            plt.show()
