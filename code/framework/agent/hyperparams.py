

class HyperParameters:

    def __init__(
        self, 
        policy: str = "MlpPolicy",
        agent_type: str = "PPO",
        # agent_type: str = "DQN", 
        # agent_type: str = "A2C", 
        total_timesteps: int = 600_000, 
        verbose: int = 1, 
        batch_size: int = 256, 
        n_eval_episodes: int = 1,
        gamma: float = 0.99,
        play_time: int = 10_000,
        learning_rate: float =  3e-4,
        n_epochs: int = 10,
        ent_coef: float = 0.01,
        exploration_fraction: float = 0.1, 
        exploration_initial_eps: float = 1.0, 
        exploration_final_eps: float = 0.05,
        
        ):
        self.policy = policy
        self.total_timesteps = total_timesteps
        self.verbose = verbose
        self.batch_size = batch_size
        self.n_eval_episodes = n_eval_episodes
        self.gamma = gamma
        self.play_time = play_time
        self.agent_type = agent_type
        self.learning_rate = learning_rate
        self.n_epochs = n_epochs
        self.ent_coef = ent_coef
        self.exploration_fraction = exploration_fraction
        self.exploration_initial_eps = exploration_initial_eps
        self.exploration_final_eps = exploration_final_eps

    def get_hyperparams(self):
        """generate printout for hyper parameters"""
        para_names = ["Policy", "Agent Type", "Total Timesteps", "Batch Size", "Gamma", "Learning Rate", \
                "# Epochs", "Entropy cef", "DQN Exploration fraction", "DQN Exploration initial eps", "DQN Exploration final eps"]
        para_values = [self.policy, self.agent_type, self.total_timesteps, self.batch_size, self.gamma, \
                self.learning_rate, self.n_epochs, self.ent_coef, self.exploration_fraction, \
                self.exploration_initial_eps, self.exploration_final_eps]
        entries = []
        for i in range(len(para_names)):
            entries.append(": ".join([para_names[i], str(para_values[i])]))
        output = "Hyperparameters\n"
        for e in entries:
            output = "\n".join([output, e])

        return "".join([output, "\n\n"])