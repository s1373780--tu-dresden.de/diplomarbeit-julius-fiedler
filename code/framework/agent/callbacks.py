import os

import numpy as np
import matplotlib.pyplot as plt
import winsound
import csv

from stable_baselines3.common import results_plotter
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy, plot_results
from stable_baselines3.common.callbacks import BaseCallback, EvalCallback, StopTrainingOnRewardThreshold
from pytimedinput import timedInput

class SaveOnBestTrainingRewardCallback(BaseCallback):
    """
    Callback for saving a model (the check is done every ``check_freq`` steps)
    based on the training reward (in practice, we recommend using ``EvalCallback``).

    :param check_freq: (int)
    :param log_dir: (str) Path to the folder where the model will be saved.
      It must contains the file created by the ``Monitor`` wrapper.
    :param verbose: (int)
    """
    def __init__(self, check_freq: int, log_dir: str, save_path: str, verbose=1, first_stop: int=300_000, agent_type: str = "PPO"):
        super(SaveOnBestTrainingRewardCallback, self).__init__(verbose)
        self.check_freq = check_freq
        self.log_dir = log_dir
        self.save_path = os.path.join(save_path, 'cb_model')
        self.best_mean_reward = -np.inf
        self.best_episode = [-1,-np.inf]
        self.mean_rewards = []
        self.when_to_ask_for_stop = first_stop
        self.agent_type = agent_type
        self.print_heading = True
        self.print_heading_weights = True

    def _init_callback(self) -> None:
        # Create folder if needed
        if self.save_path is not None:
            os.makedirs(self.save_path, exist_ok=True)

    def log_weight_updates(self):
        """log updates of neural network weights"""
        if self.agent_type == "PPO":
            if self.print_heading_weights:
                data = ["Step", "Action Net Change vs Initial", "Action Net Change vs Last",\
                     "Value Net Change vs Initial", "Value Net Change vs Last"]
                self.a_initial_weights = self.model.policy.action_net.weight.detach().numpy().flatten()
                self.v_initial_weights = self.model.policy.value_net.weight.detach().numpy().flatten()
                self.a_last_weights = self.a_initial_weights
                self.v_last_weights = self.v_initial_weights

                self.print_heading_weights = False

            else:
                a_new_weights = self.model.policy.action_net.weight.detach().numpy().flatten()
                a_scalar_product_vs_initial = np.dot(self.a_initial_weights/np.linalg.norm(self.a_initial_weights), \
                        a_new_weights/np.linalg.norm(a_new_weights))
                a_scalar_product_vs_last = np.dot(self.a_last_weights/np.linalg.norm(self.a_last_weights), \
                        a_new_weights/np.linalg.norm(a_new_weights))
                v_new_weights = self.model.policy.value_net.weight.detach().numpy().flatten()
                v_scalar_product_vs_initial = np.dot(self.v_initial_weights/np.linalg.norm(self.v_initial_weights), \
                        v_new_weights/np.linalg.norm(v_new_weights))
                v_scalar_product_vs_initial = np.dot(self.v_last_weights/np.linalg.norm(self.v_last_weights), \
                        v_new_weights/np.linalg.norm(v_new_weights))
                step = self.training_env.envs[0].env.sumo_step
                data = [str(step), str(a_scalar_product_vs_initial), str(a_scalar_product_vs_last),\
                        str(v_scalar_product_vs_initial), str(v_scalar_product_vs_initial)]

                self.a_last_weights = a_new_weights
                self.v_last_weights = v_new_weights

            abs_file_path = os.path.join(self.log_dir, "weight_updates.csv")
            with open(abs_file_path, 'a+', newline='') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
                spamwriter.writerow(data)
        

    def _on_step(self) -> bool:
        if self.training_env.buf_dones:
            # log episode data during training
            self.training_env.envs[0].env.helper.save_episode_data(self.log_dir, self.print_heading)
            self.print_heading = False
            
        if self.n_calls % self.check_freq == 0:
            self.log_weight_updates()

            # Retrieve training reward
            x, y = ts2xy(load_results(self.log_dir), 'timesteps')
            if len(x) > 0:
                # Mean training reward over the last 100 episodes
                mean_reward = np.mean(y[-100:])
                self.mean_rewards.append(mean_reward)
                if self.verbose > 0:
                    print("Num timesteps: {}".format(self.num_timesteps))
                    print("Best mean reward: {:.2f} - Last mean reward per episode: {:.2f}".format(self.best_mean_reward, mean_reward))

                # New best model, you could save the agent here
                if mean_reward > self.best_mean_reward:
                    self.best_mean_reward = mean_reward
                    # Example for saving best model
                    if self.verbose > 0:
                        print("Saving new best model to {}".format(self.save_path))
                    self.model.save(self.save_path)
                    self.best_episode = [x[-1],y[-1]]
                    
            
                if self.num_timesteps > self.when_to_ask_for_stop:
                    print(self.log_dir)
                    winsound.Beep(1000, 500)
                    value, timedOut = timedInput("continue training? (y/n)\n", 90)
                    if(timedOut):
                        self.when_to_ask_for_stop += 10_000
                    else:
                        if value == "n" or value == "no":
                            print("cb_model best episode: ", self.best_episode[0], " with return: ", self.best_episode[1])
                            print("last model episode:    ", x[-1], " with return ", y[-1])
                            return False
                        elif value == "y" or value == "yes":
                            steps = input("how many steps? \n")
                            self.when_to_ask_for_stop += int(steps)


        return True

