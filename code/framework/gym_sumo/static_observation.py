## indices of observation array


ABS_V = 0
IDX = 1
NUM_LANE = 2
MAX_SPEED = 3
# matrices delta x, delta y, rel vel, lanes
#    L  S  R
# F  2  5  8
# S  1  4  7
# B  0  3  6
# [L][Left,Same,Right][Front,Same,Back]
offset_l = 4
L_FIRST = offset_l
L_LAST = L_FIRST + 8
LLB = 0 + offset_l
LLS = 1 + offset_l
LLF = 2 + offset_l
LSB = 3 + offset_l
LSS = 4 + offset_l
LSF = 5 + offset_l
LRB = 6 + offset_l
LRS = 7 + offset_l
LRF = 8 + offset_l

offset_pa = offset_l + 9
PA_FIRST = offset_pa
PA_LAST = PA_FIRST + 4
LCR = 0 + offset_pa
DEF = 1 + offset_pa
LCL = 2 + offset_pa
ACC = 3 + offset_pa
DEC = 4 + offset_pa

NUM_OBSERVATIONS = DEC + 1 