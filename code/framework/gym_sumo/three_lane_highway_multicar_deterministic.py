from framework.gym_sumo.base_helper import BaseHelper
from framework.gym_sumo import three_lane_highway_multicar_constants as c
from framework.gym_sumo import traffic_observation as oc
import numpy as np
from gym import spaces
from collections import deque
import os
import csv

class Three_lane_highway_multicar_deterministicHelper(BaseHelper):
    
    def __init__(self, env):
        super().__init__(env)
        self.starting_positions = ["e1_0", "e1_1", "e1_2"]
        self.random_starts = False
        self.random_number = 0
        self.num_observations = c.NUM_OBSERVATIONS
        assert self.num_observations == oc.NUM_OBSERVATIONS
        self.traffic = True
        self.oc = oc
        self.c = c
        self.max_deque_len = 100
        self.vehicles_in_front = deque([], maxlen=self.max_deque_len)
        self.vehicles_slower_right = deque([], maxlen=self.max_deque_len)
        self.first_stop = self.c.FIRST_STOP_ON_CALLBACK
        self.reset_episode_data()
        self.enable_poi = False

    def get_observation_space(self):
        ### Observation space :
        # [0]		abs vel ego
        # [1]		ego lane
        # [2]		number of viable lanes    
        # [3]       speed limit
        # [4:13]    distance x
        # [13:22]   distance y
        # [22:31]   relative velocity
        # [31:40]   available lanes (distances)
        # [40:45]   possible actions: right, default, left, accel, decel
        # [45]      overtake state

        lows = np.zeros((self.num_observations,))
        highs = np.ones((self.num_observations,))
        highs[oc.ABS_V] = self.env.gym_params.max_speed
        highs[oc.IDX] = 2
        highs[oc.NUM_LANE] = 3
        highs[oc.MAX_SPEED] = self.env.gym_params.max_speed
        highs[oc.X_FIRST:oc.X_LAST+1] = np.ones(9) * 10 # see distance function in querie_RL_handler
        highs[oc.Y_FIRST:oc.Y_LAST+1] = np.ones(9) * 10 # see distance function in querie_RL_handler
        lows[oc.V_FIRST:oc.V_LAST+1] = np.ones(9) * -self.env.gym_params.max_speed
        highs[oc.V_FIRST:oc.V_LAST+1] = np.ones(9) * self.env.gym_params.max_speed
        highs[oc.L_FIRST:oc.L_LAST+1] = np.ones(9) * 10 # see distance function in querie_RL_handler
        # highs[oc.PA_FIRST:oc.PA_LAST+1] are each 1
        highs[oc.OVS] = 3
        self.observation_space = spaces.Box(low=lows, high=highs, shape=np.shape(lows), dtype=float) 
        
        return self.observation_space
    
    def print_observations_og(self):
        # originally:
        # 2 5 8
        # 1 4 7
        # 0 3 6
        obs = np.round(self.env.observation, 1)
        print(obs[oc.ABS_V:oc.MAX_SPEED+1], obs[oc.OVS])
        x = obs[oc.X_FIRST:oc.X_LAST+1]
        y = obs[oc.Y_FIRST:oc.Y_LAST+1]
        x = [i if i != 0 else " - " for i in x]
        y = [i if i != 0 else " - " for i in y]
        v = obs[oc.V_FIRST:oc.V_LAST+1]
        l = obs[oc.L_FIRST:oc.L_LAST+1]
        l = [i if i != 0 else " - " for i in l]
        print("x ", x[2], x[5], x[8], "  y ", y[2], y[5], y[8], "  v ", v[2], v[5], v[8], "  lane ", l[2], l[5], l[8])
        print("x ", x[1], x[4], x[7], "  y ", y[1], y[4], y[7], "  v ", v[1], v[4], v[7], "  lane ", l[1], l[4], l[7])
        print("x ", x[0], x[3], x[6], "  y ", y[0], y[3], y[6], "  v ", v[0], v[3], v[6], "  lane ", l[0], l[3], l[6])
        pa = np.array(obs[oc.PA_FIRST:oc.PA_LAST+1], dtype=int)
        print("r", pa[0], "  d", pa[1], "  l", pa[2], "  ac", pa[3], "  dc", pa[4])

    def print_observations(self):
        # originally:
        # 2 5 8
        # 1 4 7
        # 0 3 6
        obs = np.round(self.env.observation, 1)
        print("v_abs: ", obs[oc.ABS_V], " v_max: ", obs[oc.MAX_SPEED], " lane: ", obs[oc.IDX], " #lanes: ", obs[oc.NUM_LANE])
        x = obs[oc.X_FIRST:oc.X_LAST+1]
        y = obs[oc.Y_FIRST:oc.Y_LAST+1]
        x = [i if i != 0 else " - " for i in x]
        y = [i if i != 0 else " - " for i in y]
        v = obs[oc.V_FIRST:oc.V_LAST+1]
        l = obs[oc.L_FIRST:oc.L_LAST+1]
        l = [i if i != 0 else " - " for i in l]
        print("x ", x[0], x[1], x[2], "  y ", y[0], y[1], y[2], "  v_rel ", v[0], v[1], v[2], "  lane ", l[0], l[1], l[2])
        print("x ", x[3], x[4], x[5], "  y ", y[3], y[4], y[5], "  v_rel ", v[3], v[4], v[5], "  lane ", l[3], l[4], l[5])
        print("x ", x[6], x[7], x[8], "  y ", y[6], y[7], y[8], "  v_rel ", v[6], v[7], v[8], "  lane ", l[6], l[7], l[8])
        pa = np.array(obs[oc.PA_FIRST:oc.PA_LAST+1], dtype=int)
        print("right:", pa[0], "  default:", pa[1], "  left:", pa[2], "  accelerate:", pa[3], "  decelerate:", pa[4])

    def rand(self):
        if self.random_starts:
            self.random_number = np.random.randint(0, len(self.starting_positions))
        else:
            self.random_number = 0
    
    def get_observe_possible_actions(self):
        return self.c.OBSERVE_POSSIBLE_ACTIONS

    def get_action_filtering(self):
        return self.c.ACTION_FILTERING

    def get_starting_edge_ego(self):
        return "e1"
    
    def get_starting_lane_ego(self):
        return "e1_0"

    def get_route_ego(self):
        n = self.starting_positions[self.random_number].split("_")[0].split("e")[1]
        return "_".join(["route", n])
        
    def get_starting_position_on_lane(self):
        return 100
    
    def get_depart_speed(self):
        return self.c.EGO_DEPART_SPEED

    def get_ID_Type_ego(self):
        return "car"    

    # def add_obstacle_vehicles(self):
    #     for i in range(3):
    #         veh_name = str(-i-1)
    #         depart_lane = str(i%2)
    #         depart_pos = np.random.randint(50, 70)
    #         depart_speed = np.random.randint(8, 14)
    #         try:
    #             self.env.con.vehicle.add(veh_name, "route_1", typeID="fast_car", depart=None, departPos=depart_pos,\
    #                                 departSpeed=depart_speed, departLane=depart_lane)
    #             self.env.con.polygon.add("gap_"+veh_name, [(0,0), (1,1)], (0,255,100), layer=10)
    #         except:
    #             pass
    #         # self.env.con.vehicle.setLaneChangeMode(veh_name, 0)
    #         self.env.con.vehicle.setSpeed(veh_name, depart_speed)
    #         # self.env.con.vehicle.setSpeedMode(veh_name, 0)
    #         self.env.con.vehicle.setColor(veh_name, (0, 255, 0))
    #     for i in range(3):
    #         depart_lane = str(i%2)
    #         veh_name = str(i+1)
    #         depart_pos = np.random.randint(120, 300)
    #         depart_speed = np.random.randint(1, 4)
    #         try:
    #             self.env.con.vehicle.add(veh_name, "route_1", typeID="slow_car", depart=None, departPos=depart_pos,\
    #                                 departSpeed=depart_speed, departLane=depart_lane)
    #             self.env.con.polygon.add("gap_"+veh_name, [(0,0), (1,1)], (0,255,100), layer=10)
    #         except:
    #             pass
    #         # self.env.con.vehicle.setLaneChangeMode(veh_name, 0)
    #         self.env.con.vehicle.setSpeed(veh_name, depart_speed)
    #         # self.env.con.vehicle.setSpeedMode(veh_name, 0)
    #         self.env.con.vehicle.setColor(veh_name, (0, 255, 0))

    # def redistribute_vehicles(self):
    #     ego_pos = self.env.con.vehicle.getLanePosition("1000")
    #     for veh in np.array(self.env.con.vehicle.getIDList())[np.array(self.env.con.vehicle.getIDList())!="1000"]:
    #         veh_pos = self.env.con.vehicle.getLanePosition(veh)  
    #         if abs(veh_pos - ego_pos) > 110:
    #             if np.random.randint(0,2):
    #                 new_pos = np.random.randint(ego_pos +30, ego_pos +60)
    #             else:
    #                 new_pos = np.random.randint(ego_pos -60, ego_pos -30)
    #             self.env.con.vehicle.moveTo(veh, "e1_0", new_pos)
    #             self.filter_deques([veh])
    
    def add_obstacle_vehicles(self):
        self.scenario = np.random.choice([0,1,2], size=1, p=c.SCENARIO_PROBABILITIES)
        # 0 random cars
        if self.scenario == 0:
            
            veh_name = "-1"
            depart_lane = "2"
            depart_pos = 50
            depart_speed = 13
            self.add_car(veh_name, "fast_car", depart_pos, depart_speed, depart_lane, False, False)

            veh_name = "-2"
            depart_lane = "1"
            depart_pos = 70
            depart_speed = 10
            self.add_car(veh_name, "fast_car", depart_pos, depart_speed, depart_lane, False, False)

            veh_name = "1"
            depart_lane = "0"
            depart_pos = 130
            depart_speed = 4
            self.add_car(veh_name, "slow_car", depart_pos, depart_speed, depart_lane, False, False)

            veh_name = "2"
            depart_lane = "1"
            depart_pos = 150
            depart_speed = 3
            self.add_car(veh_name, "slow_car", depart_pos, depart_speed, depart_lane, True, True)
        
        # 1 "Mittelspurschleicher"
        if self.scenario == 1:
            for i in range(2):
                veh_name = str(-i-1)
                depart_lane = str(i%2)
                depart_pos = np.random.randint(50, 70)
                depart_speed = np.random.randint(8, 16)
                self.add_car(veh_name, "fast_car", depart_pos, depart_speed, depart_lane, False, False)
            
            veh_name = "1"
            depart_lane = "1"
            depart_pos = np.random.randint(120, 180)
            depart_speed = np.random.randint(1, 7)
            self.add_car(veh_name, "fast_car", depart_pos, depart_speed, depart_lane, False, True)

            veh_name = "2"
            depart_lane = "0"
            depart_pos = np.random.randint(120, 300)
            depart_speed = np.random.randint(1, 5)
            self.add_car(veh_name, "slow_car", depart_pos, depart_speed, depart_lane, False, False)
        
        # 2 fast car from behind
        elif self.scenario == 2:
            # two slow vehicle in front
            depart_lane = "1"
            veh_name = "1"
            depart_pos = 130
            depart_speed = 3
            self.add_car(veh_name, "slow_car", depart_pos, depart_speed, depart_lane, True, True)
            
            depart_lane = "2"
            veh_name = "2"
            depart_pos = 140
            depart_speed = 3
            self.add_car(veh_name, "slow_car", depart_pos, depart_speed, depart_lane, True, True)

            # fast vehicle from behind
            depart_lane = "2"
            veh_name = "-1"
            depart_pos = 75
            depart_speed = 8
            self.add_car(veh_name, "fast_car", depart_pos, depart_speed, depart_lane, True, True)

            depart_lane = "0"
            veh_name = "-2"
            depart_pos = 10
            depart_speed = 5
            self.add_car(veh_name, "fast_car", depart_pos, depart_speed, depart_lane, False, False)


    def redistribute_vehicles(self):
        ego_pos = self.env.con.vehicle.getLanePosition("1000")
        # 0 random cars
        if self.scenario == 0:
            for veh in np.array(self.env.con.vehicle.getIDList())[np.array(self.env.con.vehicle.getIDList())!="1000"]:
                veh_pos = self.env.con.vehicle.getLanePosition(veh)  
                # if veh far ahead or ego somewhat ahead
                if veh_pos - ego_pos > 100 or ego_pos - veh_pos > 60:
                    # place slow car in front and fast car behind
                    if self.env.con.vehicle.getTypeID(veh) == "slow_car":
                        new_pos = ego_pos + 50
                    else:
                        new_pos = ego_pos - 50
                        
                    if veh == "1":
                        new_lane = "e1_0"
                    if veh == "2":
                        new_lane = "e1_2"
                    if veh == "-1":
                        new_lane = "e1_2"
                    if veh == "-2":
                        new_lane = "e1_1"
                    
                    self.env.con.vehicle.moveTo(veh, new_lane, new_pos)
                    self.filter_deques([veh])
        # 1 "Mittelspurschleicher"
        elif self.scenario == 1:
            if abs(ego_pos - self.env.con.vehicle.getLanePosition("1")) > 100:
                new_pos = np.random.randint(ego_pos +50, ego_pos +90)
                self.env.con.vehicle.moveTo("1", "e1_1", new_pos)
                self.env.con.vehicle.setSpeed("1", np.random.randint(1,7))
                self.filter_deques(["1"])
            if abs(ego_pos - self.env.con.vehicle.getLanePosition("2")) > 100:
                new_pos = np.random.randint(ego_pos +50, ego_pos +90)
                self.env.con.vehicle.moveTo("2", "e1_0", new_pos)
                self.env.con.vehicle.setSpeed("2", np.random.randint(1,5))
                self.filter_deques(["2"])
            if abs(ego_pos - self.env.con.vehicle.getLanePosition("-1")) > 100:
                new_pos = np.random.randint(ego_pos -70, ego_pos -50)
                self.env.con.vehicle.moveTo("-1", "e1_2", new_pos)
                self.env.con.vehicle.setSpeed("-1", np.random.randint(8,16))
                self.filter_deques(["-1"])
            if abs(ego_pos - self.env.con.vehicle.getLanePosition("-2")) > 100:
                new_pos = np.random.randint(ego_pos -50, ego_pos -30)
                self.env.con.vehicle.moveTo("-2", "e1_1", new_pos)
                self.env.con.vehicle.setSpeed("-2", np.random.randint(8,16))
                self.filter_deques(["-2"])

        # 2 fast car from behind
        elif self.scenario == 2:
            if abs(ego_pos - self.env.con.vehicle.getLanePosition("1")) > 100:                
                new_pos = np.random.randint(ego_pos +50, ego_pos +90)
                self.env.con.vehicle.moveTo("1", "e1_0", new_pos)
                self.env.con.vehicle.setSpeed("1", np.random.randint(1,7))
                self.filter_deques(["1"])
            if abs(ego_pos - self.env.con.vehicle.getLanePosition("2")) > 100:
                new_pos = np.random.randint(ego_pos +50, ego_pos +90)
                self.env.con.vehicle.moveTo("2", "e1_1", new_pos)
                self.env.con.vehicle.setSpeed("2", np.random.randint(1,7))
                self.filter_deques(["2"])
            if abs(ego_pos - self.env.con.vehicle.getLanePosition("-1")) > 100:
                new_pos = np.random.randint(ego_pos -70, ego_pos -50)
                self.env.con.vehicle.moveTo("-1", "e1_2", new_pos)
                self.env.con.vehicle.setSpeed("-1", np.random.randint(8,16))
                self.filter_deques(["-1"])
            if abs(ego_pos - self.env.con.vehicle.getLanePosition("-2")) > 100:
                new_pos = np.random.randint(ego_pos -50, ego_pos -30)
                self.env.con.vehicle.moveTo("-2", "e1_1", new_pos)
                self.env.con.vehicle.setSpeed("-2", np.random.randint(8,16))
                self.filter_deques(["-2"])

        
        

    def get_reward(self):
        terminal = False
        info = dict()

        # reward for change of state
        reward_step = self.get_reward_step()

        # keeping right bonus
        reward_keeping_right = self.get_reward_right()

        # valid action?
        reward_invalid_action, info_ = self.get_reward_invalid_action()
        info["invalid action"] = info_

        # Speed Reward 
        reward_speed, info_ = self.get_reward_speed()
        info["speed"] = info_

        # Safety Gap violation?
        terminal_, reward_safety_gap, info_ = self.get_reward_safety_gap()
        info["safety gap violation"] = info_
        if terminal_:
            terminal = True

        # Overtaking on the right
        reward_right_overtaking, info_ = self.get_reward_right_overtaking()
        info["right overtake"] = info_

        # Collision check
        terminal_, reward_collision, info_ = self.get_reward_collision()
        info["collision"] = info_
        if terminal_:
            terminal = True

        # end of road check
        terminal_, reward_end_lane, info_ = self.get_reward_end_of_lane()
        info["end of road"] = info_
        if terminal_:
            terminal = True

        # successfull overtake on a car
        reward_successful_overtaking, info_ = self.get_reward_successful_overtaking()
        info["successful overtake"] = info_

        # # offroad check
        terminal_, reward_offroad, info_ = self.get_reward_offroad()
        info["offroad"] = info_
        if terminal_:
            terminal = True
        else:
            self.env.off_road = False

        # get success reward
        terminal_, reward_success, info_ = self.get_reward_success()
        info["success"] = info_

        reward  = reward_step \
                + reward_speed  \
                + reward_keeping_right  \
                + reward_safety_gap \
                + reward_right_overtaking\
                + reward_successful_overtaking\
                + reward_offroad \
                + reward_collision  \
                + reward_success  \
                + reward_end_lane \
                + reward_invalid_action

        self.save_step_data(reward, reward - reward_invalid_action, info) # in order to compare with and without OBSERVE_POSSIBLE_ACTIONS
        
        if terminal_:
            terminal = True
            print(f"Successfully completed 1 Episode")

        elif self.env.sumo_steps_since_last_reset > 10000:
            terminal = True
            print("vehicle probably stuck, reset")

        if terminal:
            self.env.episode += 1

        return reward, terminal, info

    # def get_reward_right_overtaking(self):
    #     # vehicle directly to my left
    #     if self.env.observation[oc.VLS] < 0:
    #         return self.c.REW_RIGHT_OVERTAKING, True
    #     else:
    #         return 0, False
    def get_reward_right_overtaking(self):
        # vehicle directly to my left or left in front and slower
        length = 5                
        self.safety_gap_long_front = max(self.env.observation[oc.ABS_V]*3.6 / 2 , 5)
        dist_value = 0.001*(length + self.safety_gap_long_front - 100) ** 2
        if self.env.observation[oc.VLS] < 0 or \
                (self.env.observation[oc.VLF] <0 and self.env.observation[oc.XLF] > dist_value):
            return self.c.REW_RIGHT_OVERTAKING, True
        # vehicle right and faster    
        elif self.env.observation[oc.VRS] > 0:
            return self.c.REW_RIGHT_OVERTAKING, True
        else:
            return 0, False

    def get_reward_invalid_action(self):
        if self.c.OBSERVE_POSSIBLE_ACTIONS:
            if self.env.action_was_valid:
                return 0, False
            else:
                return self.c.REW_INVALID_ACTION, True
        else:
            return 0, False

    def visualize_safety_gaps(self):
        for vehicle in self.env.con.vehicle.getIDList():
            try:
                safety_gap_long = self.env.con.vehicle.getSpeed(vehicle) * 3.6 / 2.0
                pos = self.env.con.vehicle.getPosition(vehicle)
                self.env.con.polygon.setShape("gap_"+vehicle, [(pos[0], pos[1]), (pos[0]+ safety_gap_long, pos[1])])
            except:
                try:
                    self.env.con.polygon.add("gap_"+vehicle, [(pos[0], pos[1]), (pos[0]+ safety_gap_long, pos[1])], (0,255,100), layer=10)
                except:
                    pass
        
        # print some key observations on simulation screen
        #TODO These observations are always one step behind the simulation!!
        if self.enable_poi == True:
            ego_pos = self.env.con.vehicle.getPosition("1000")
            for id in self.env.con.poi.getIDList():
                try:
                    self.env.con.poi.remove(id)
                except: pass
            top = 30
            if self.scenario is not None:
                self.env.con.poi.add("scenario: "+str(self.scenario), ego_pos[0], ego_pos[1] + top, color=(0,255,100))
            self.env.con.poi.add("vel: "+str(round(self.env.current_speed,1)), ego_pos[0], ego_pos[1]+ top-5, color=(0,255,100))
            self.env.con.poi.add("max vel: "+str(round(self.env.observation[oc.MAX_SPEED],1)), ego_pos[0], ego_pos[1]+top-10, color=(0,255,100))
            self.env.con.poi.add("last action: "+self.env.action_to_string(self.env.chosen_action), ego_pos[0], ego_pos[1]+top-15, color=(0,255,100))
            pa = np.array(self.env.observation[oc.PA_FIRST:oc.PA_LAST+1], dtype=int)
            legal_action_str = "r"+ str(pa[0]) + "  d" + str(pa[1]) + "  l" + str(pa[2]) + "  ac" + str(pa[3]) + "  dc" + str(pa[4])
            self.env.con.poi.add("legal actions: "+legal_action_str, ego_pos[0], ego_pos[1]+top-45, color=(0,255,100))


    def get_reward_safety_gap(self):
        """to do this properly, one would need the vehicle lengths
        we assume here, that all vehicles are of length 5
        """
        violation = False

        ## longitudinal
        length = 5             
        # front or same cell:
        # "halber Tacho"
        self.safety_gap_long_front = max(self.env.observation[oc.ABS_V]*3.6 / 2 , 5)
        dist_value = 0.001*(length + self.safety_gap_long_front - 100) ** 2
        if self.env.observation[oc.XSF] > dist_value or self.env.observation[oc.XSS] > dist_value:
            violation = True
        
        # back: (calc vel of veh beind -> gap)
        self.safety_gap_long_back = max((self.env.observation[oc.ABS_V] + self.env.observation[oc.VSB]) *3.6 / 2 , 5)
        dist_value = 0.001*(length + self.safety_gap_long_back - 100) ** 2
        if self.env.observation[oc.XSB] > dist_value:
            violation = True
        
        ## lateral
        safety_gap_lateral = 2.5
        dist_value = 0.001*(safety_gap_lateral - 100) ** 2
        if self.env.observation[oc.YLS] > dist_value or self.env.observation[oc.YRS] > dist_value or self.env.observation[oc.YSS] > dist_value:
            violation = True

        if violation:
            return self.c.RESET_ON_SAFETY_GAP_VIO, self.c.REW_SAFETY_GAP_VIO, True
        else:
            return False, 0, False

    def get_reward_collision(self):
        if self.env.observation[self.oc.XSS] or self.env.observation[self.oc.YSS]:
            if self.env.training:
                return self.c.RESET_ON_COL, self.c.REW_COL, True
            else:
                return True, self.c.REW_COL, True
        else: 
            return False, 0, False
    
    def get_reward_offroad(self):
        if self.env.off_road:
            if self.env.training:
                return self.c.RESET_ON_OFFROAD, self.c.REW_OFFROAD, True
            else:
                return True, self.c.REW_OFFROAD, True
        else: 
            return False, 0, False
        
    def get_reward_success(self):
        ego_pos = self.env.con.vehicle.getLanePosition("1000")
        if ego_pos > 1100: # after 1km drive
            return self.c.RESET_ON_SUCCESS, self.c.REW_SUCCESS, True
        else: 
            return False, 0, False

    # def get_reward_right(self):
    #     lane_index = self.env.observation[self.oc.IDX]
    #     if lane_index == 0:
    #         return 0
    #     else:
    #         return self.c.REW_NOT_RIGHT - (lane_index -1) * 0.5
    def get_reward_right(self):
        lane_index = self.env.observation[self.oc.IDX]
        if lane_index != 0 and self.env.observation[oc.LCR]:
            if lane_index == 1:
                return self.c.REW_MIDDLE
            elif lane_index == 2:
                return self.c.REW_LEFT
            else:
                return #error
        else:
            return 0  

    def get_reward_speed(self):
        if self.env.observation[self.oc.ABS_V] < 5:
            return self.c.REW_LOW_SPEED, "slow"
        elif self.env.observation[self.oc.ABS_V] > self.env.observation[self.oc.MAX_SPEED]: 
            return self.c.REW_SPEEDING, "speeding"
        else:
            return 0, "normal"
    
    def get_reward_end_of_lane(self):
        if self.env.observation[oc.LSS] == 10:
            return self.c.RESET_ON_END_LANE, self.c.REW_END_LANE, True
        else:
            return False, 0, False

    def get_reward_step(self):
        if self.env.chosen_action == 1:
            return self.c.REW_STEP_DEFAULT
        else:
            return self.c.REW_STEP_ACTION

    def get_overtake_state(self):
        state_0, state_1, state_2, state_3 = 0, 0 , 0, 0
        for i in [oc.XSF, oc.XRF]:
            if self.env.observation[i] > 0:
                self.vehicles_in_front.append(self.env.close_vehicles[i-4])
        if self.env.observation[oc.XRS] > 9.5 and self.env.observation[oc.YRS] > 0 and self.env.observation[oc.VRS] < 0:
            self.vehicles_slower_right.append(self.env.close_vehicles[7])
        
        ego_pos = self.env.con.vehicle.getLanePosition("1000") 
        for veh in self.env.con.vehicle.getIDList():
            veh_pos = self.env.con.vehicle.getLanePosition(veh)
            if veh in self.vehicles_in_front and veh in self.vehicles_slower_right \
                    and ego_pos > veh_pos + self.safety_gap_long_back: 
                state_3 = 3
                if self.env.enable_debug_prints:
                    print("overtake on ", veh, " successful")
                self.vehicles_in_front = deque(filter((veh).__ne__, self.vehicles_in_front), maxlen=self.max_deque_len)
                self.vehicles_slower_right = deque(filter((veh).__ne__, self.vehicles_slower_right), maxlen=self.max_deque_len)
            elif veh in self.vehicles_in_front and veh in self.vehicles_slower_right:
                state_2 = 2
            elif veh in self.vehicles_in_front:
                state_1 = 1
        
        return max([state_0, state_1, state_2, state_3])

    def get_reward_successful_overtaking(self):
        state = self.env.observation[oc.OVS]
        if state == 2:
            return 2, 2
        elif state == 3:
            return self.c.REW_SUCCESSFULL_OVERTAKING, 3
        else:
            return 0, False
    
    def filter_deques(self, filter_list=[]):
        if len(filter_list) == 0:
            self.vehicles_in_front = deque([], maxlen=self.max_deque_len)
            self.vehicles_slower_right = deque([], maxlen=self.max_deque_len)
        else:
            for veh in filter_list:
                self.vehicles_in_front = deque(filter((veh).__ne__, self.vehicles_in_front), maxlen=self.max_deque_len)
                self.vehicles_slower_right = deque(filter((veh).__ne__, self.vehicles_slower_right), maxlen=self.max_deque_len)

    def add_car(self, veh_name, typeID, depart_pos, depart_speed, depart_lane, speed_mode_0, lanechange_mode_0):
        try:
            self.env.con.vehicle.add(veh_name, "route_1", typeID=typeID, depart=None, departPos=depart_pos,\
                                departSpeed=depart_speed, departLane=depart_lane)
            self.env.con.polygon.add("gap_"+veh_name, [(0,0), (1,1)], (0,255,100), layer=10)
            if lanechange_mode_0:
                self.env.con.vehicle.setLaneChangeMode(veh_name, 0)
            self.env.con.vehicle.setSpeed(veh_name, depart_speed)
            if speed_mode_0:
                self.env.con.vehicle.setSpeedMode(veh_name, 0)
            self.env.con.vehicle.setColor(veh_name, (0, 255, 0))
        except:
            pass

    def save_step_data(self, reward, reward_compare, info):
        self.episode_data["episode_length"] += 1
        self.episode_data["total_reward"] += reward
        self.episode_data["total_reward_compare"] += reward_compare
        self.episode_data["no_speeding"] += info["speed"] == "speeding"
        self.episode_data["no_right_overtake"] += info["right overtake"]
        self.episode_data["no_successful_overtake"] += info["successful overtake"]
        self.episode_data["no_safety_gap_violation"] += info["safety gap violation"]
        self.episode_data["no_collision"] += info["collision"]
        self.episode_data["no_offroad"] += info["offroad"]
        self.episode_data["no_left"] += self.env.observation[oc.IDX] == 2
        self.episode_data["no_middle"] += self.env.observation[oc.IDX] == 1
        self.episode_data["no_right"] += self.env.observation[oc.IDX] == 0
        self.episode_data["total_velocity"] += self.env.observation[oc.ABS_V]
        self.episode_data["no_invalid_action"] += info["invalid action"]
        self.episode_data["invalid_action_led_to_terminal"] += int(info["invalid action"] and (info["collision"] or info["offroad"]))

    def save_episode_data(self, path, print_heading):
        """save the episode data
        this gets called by callback during training"""
        
        abs_file_path = os.path.join(path, "training_data.csv")
        with open(abs_file_path, 'a+', newline='') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
            if print_heading:
                keys = ["Step", "Episode"]
                for k in self.episode_data.keys():
                    keys.append(str(k))
                spamwriter.writerow(keys)
            values = [str(self.env.sumo_step), str(self.env.episode)]
            for v in self.episode_data.values():
                values.append(str(v))
            spamwriter.writerow(values)

        self.reset_episode_data()

    def reset_episode_data(self):
        self.episode_data = dict(episode_length=0, total_reward=0, total_reward_compare=0, no_speeding=0, no_right_overtake=0, no_successful_overtake=0,\
            no_safety_gap_violation=0, no_collision=0, no_offroad=0, no_left=0, no_middle=0, no_right=0, total_velocity=0, no_invalid_action=0, invalid_action_led_to_terminal=0)
