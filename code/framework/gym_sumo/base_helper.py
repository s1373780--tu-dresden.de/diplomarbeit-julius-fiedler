from abc import ABC, abstractmethod



class BaseHelper(ABC):

    def __init__(self, env):
        self.env = env

    @abstractmethod
    def get_action_filtering(self):
        raise NotImplementedError

    @abstractmethod
    def get_starting_edge_ego(self):
        raise NotImplementedError

    @abstractmethod
    def get_starting_lane_ego(self):
        raise NotImplementedError

    @abstractmethod
    def get_route_ego(self):
        raise NotImplementedError

    @abstractmethod
    def get_ID_Type_ego(self):
        raise NotImplementedError
    
    @abstractmethod
    def add_obstacle_vehicles(self, ids):
        raise NotImplementedError

    @abstractmethod
    def redistribute_vehicles(self):
        raise NotImplementedError

    @abstractmethod
    def get_reward(self):
        raise NotImplementedError