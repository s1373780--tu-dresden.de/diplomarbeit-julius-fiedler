
class GymParameters():
    def __init__(self,
    ego_vehicle_id: str="1000",
    simulation_type: str="three_lane_circle",
    mode: str = "gui",
    max_speed: int = 15, # m/s
    context_range: int = 100,
    # n_steps_to_success: int = 500,
    # reward_speed_factor: float = 1.0,
    # reward_keeping_right_factor: float = 1.0,
    # reward_collision_factor: float = 1.0,
    # reward_off_road_factor: float = 1.0,
    # reward_endurance_factor: float = 1.0,
    # n_simulation_steps_per_action: int = 1,
    # number_of_observations: int = 26,
    number_of_actions: int = 5,
    seed: int = 1

    ):
        self.ego_vehicle_id = ego_vehicle_id
        self.simulation_type = simulation_type
        self.mode = mode
        self.max_speed = max_speed
        self.context_range = context_range
        self.steps_per_sec = None
        self.pubs_per_sec = None
        # self.n_steps_to_success = n_steps_to_success
        # self.reward_speed_factor = reward_speed_factor
        # self.reward_keeping_right_factor = reward_keeping_right_factor
        # self.reward_collision_factor = reward_collision_factor
        # self.reward_off_road_factor = reward_off_road_factor
        # self.reward_endurance_factor = reward_endurance_factor
        # self.n_simulation_steps_per_action = n_simulation_steps_per_action
        # self.number_of_observations = number_of_observations
        self.number_of_actions = number_of_actions
        self.seed = seed