## indices of observation array


ABS_V = 0
IDX = 1
NUM_LANE = 2
MAX_SPEED = 3
# matrices delta x, delta y, rel vel, lanes
#    L  S  R
# F  2  5  8
# S  1  4  7
# B  0  3  6
# [X,Y,V,L][Left,Same,Right][Front,Same,Back]
offset_x = 4
X_FIRST = offset_x
X_LAST = X_FIRST + 8
XLB = 0 + offset_x
XLS = 1 + offset_x
XLF = 2 + offset_x
XSB = 3 + offset_x
XSS = 4 + offset_x
XSF = 5 + offset_x
XRB = 6 + offset_x
XRS = 7 + offset_x
XRF = 8 + offset_x

offset_y = offset_x + 9
Y_FIRST = offset_y
Y_LAST = Y_FIRST + 8
YLB = 0 + offset_y
YLS = 1 + offset_y
YLF = 2 + offset_y
YSB = 3 + offset_y
YSS = 4 + offset_y
YSF = 5 + offset_y
YRB = 6 + offset_y
YRS = 7 + offset_y
YRF = 8 + offset_y

offset_v = offset_y + 9
V_FIRST = offset_v
V_LAST = V_FIRST + 8
VLB = 0 + offset_v
VLS = 1 + offset_v
VLF = 2 + offset_v
VSB = 3 + offset_v
VSS = 4 + offset_v
VSF = 5 + offset_v
VRB = 6 + offset_v
VRS = 7 + offset_v
VRF = 8 + offset_v

offset_l = offset_v + 9
L_FIRST = offset_l
L_LAST = L_FIRST + 8
LLB = 0 + offset_l
LLS = 1 + offset_l
LLF = 2 + offset_l
LSB = 3 + offset_l
LSS = 4 + offset_l
LSF = 5 + offset_l
LRB = 6 + offset_l
LRS = 7 + offset_l
LRF = 8 + offset_l

offset_pa = offset_l + 9
PA_FIRST = offset_pa
PA_LAST = PA_FIRST + 4
LCR = 0 + offset_pa
DEF = 1 + offset_pa
LCL = 2 + offset_pa
ACC = 3 + offset_pa
DEC = 4 + offset_pa

OVS = DEC + 1

NUM_OBSERVATIONS = OVS + 1 