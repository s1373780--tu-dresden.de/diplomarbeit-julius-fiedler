import os
import sys
import gym
import numpy as np
import struct
import time
import traci
from traci import constants as c

from gym import Env
from gym import error, spaces, utils
from gym.utils import seeding

from framework.errors.errors import *
from framework.gym_sumo.gym_parameters import GymParameters
import framework.tests.constants as const
from framework.gym_sumo.circle_helper import CircleHelper
from framework.gym_sumo.tf_dd_helper import TfDdHelper
from framework.gym_sumo.scenario_1_helper import Scenario1Helper
from framework.gym_sumo.scenario_2_helper import Scenario2Helper
from framework.gym_sumo.narrow_helper import NarrowHelper
from framework.gym_sumo.three_lane_highway import Three_lane_highwayHelper
from framework.gym_sumo.three_lane_highway_multicar import Three_lane_highway_multicarHelper
from framework.gym_sumo.three_lane_highway_multicar_deterministic import Three_lane_highway_multicar_deterministicHelper

class SUMOEnvOnto(Env):
	'''
	all units SI, velocities in m/s !
	'''


	def __init__(self, con: traci.connection, gym_params: GymParameters):

		# debug Parameters
		self.index_list = []
		
		# passed Parameters
		self.con = con
		self.ego_vehicle_id = gym_params.ego_vehicle_id
		self.gym_params = gym_params
		self.synchronization_cycle = self.gym_params.steps_per_sec / self.gym_params.pubs_per_sec

		# select helper
		if self.gym_params.simulation_type == const.THREE_LANE_CIRCLE:
			self.helper = CircleHelper(self)
		elif self.gym_params.simulation_type == const.TESTFELD_DD:
			self.helper = TfDdHelper(self)
		elif self.gym_params.simulation_type == const.SCENARIO_1:
			self.helper = Scenario1Helper(self)
		elif self.gym_params.simulation_type == const.SCENARIO_2:
			self.helper = Scenario2Helper(self)
		elif self.gym_params.simulation_type == const.NARROW:
			self.helper = NarrowHelper(self)
		elif self.gym_params.simulation_type == const.THREE_LANE_HIGHWAY:
			self.helper = Three_lane_highwayHelper(self)
		elif self.gym_params.simulation_type == const.THREE_LANE_HIGHWAY_MULTICAR:
			self.helper = Three_lane_highway_multicarHelper(self)
		elif self.gym_params.simulation_type == const.THREE_LANE_HIGHWAY_MULTICAR_DETERMINISTIC:
			self.helper = Three_lane_highway_multicar_deterministicHelper(self)
		else:
			raise MissingHelperClassError("no helper class for this sumo config")

		# Environment parameters
		self.num_actions = self.gym_params.number_of_actions
		self.num_observations = self.helper.num_observations
		self.current_speed = self.helper.get_depart_speed()
		self.observation = np.zeros((self.num_observations,))
		self.seed(self.gym_params.seed)
		self.sumo_step = 0 		# count number ob steps actually taken
		self.sumo_steps_since_last_reset = 0 
		self.episode = 0
		self.simulation_started = False
		self.reset()
		self.last_observation_update_list = np.zeros(6)  ##TODO make this number dynamic
		self.last_observation_request = 0
		self.done = False
		self.off_road = False
		self.impossible_action_taken = False
		self.runner = None
		self.stuck_counter = 0
		self.first_step = None
		self.request_observation_now_flag = True
		self.enable_debug_prints = False
		self.enable_action_filtering = self.helper.get_action_filtering()
		self.close_vehicles = None

		# use this to distinguish between training and validation:
		self.training = None

		
		# Action and Observation space
		self.action_space = spaces.Discrete(self.num_actions)
		print("action space: ", self.action_space)

		self.observation_space = self.helper.get_observation_space()
		print("observation space: ", self.observation_space)

		
	def seed(self, seed):
		np.random.seed(seed)

	
	def _observation(self):
		"""send update request for observation data, wait for new data, synchronize with simulation
		Data goes: SUMO->DataHandler->Onology->Runner->here
		Returns: observation
		"""
		if self.simulation_started:
			sumo_time_stamp = self.con.simulation.getTime()
			if self.sumo_step % self.synchronization_cycle == 0:  
				self.runner.request_update()
				self.last_observation_request = sumo_time_stamp
			
			# wait for synchronization between observation and env
			if sumo_time_stamp >= self.last_observation_request + np.round(1/self.gym_params.pubs_per_sec - 1/self.gym_params.steps_per_sec, 3):   
				t = time.time()
				timing = 10
				while not self.synchronized():
					time.sleep(0.001)  
					if time.time() > t + 10:
						if int(time.time()-t) > timing:
							timing += 10
							print("Waiting for Ontology, resending update request.....")
							self.runner.request_update()
			# TODO: due to limited excess to roadrunner, the xodr has no variable speed limits,
        	# TODO: but those are needed and implemented here
			if self.observation[self.helper.oc.ABS_V] +1 > self.observation[self.helper.oc.MAX_SPEED]:
				self.observation[self.helper.oc.ACC] = False
			if self.observation[self.helper.oc.ABS_V]  > self.observation[self.helper.oc.MAX_SPEED]:
				self.observation[self.helper.oc.DEF] = False
			
			if self.helper.traffic:
				self.color_closest_vehicles()
				# this can not be done by ontology due to teleporting vehicles
				self.observation[self.helper.oc.OVS] = self.helper.get_overtake_state()
		return self.observation
		
	# different update methods are called from runner for different topics

	def update_observations(self, time_stamp, delta_x, delta_y, rel_v, lane, occupation):
		self.last_observation_update_list[0] = time_stamp
		if self.helper.traffic:
			self.observation[self.helper.oc.X_FIRST:self.helper.oc.X_LAST+1] = delta_x
			self.observation[self.helper.oc.Y_FIRST:self.helper.oc.Y_LAST+1] = delta_y
			self.observation[self.helper.oc.V_FIRST:self.helper.oc.V_LAST+1] = rel_v
			self.observation[self.helper.oc.L_FIRST:self.helper.oc.L_LAST+1] = lane
			# self.observation[37:45] = np.delete(occupation, 4)
		else:
			self.observation[self.helper.oc.L_FIRST:self.helper.oc.L_LAST+1] = lane

	def update_current_lane_index(self, time_stamp, sumo_idx, drivable_lanes):
		self.last_observation_update_list[1] = time_stamp
		self.observation[self.helper.oc.IDX] = sumo_idx
		self.observation[self.helper.oc.NUM_LANE] = drivable_lanes
	
	def update_possible_actions(self, time_stamp, possible_actions):
		self.last_observation_update_list[2] = time_stamp
		if self.helper.get_observe_possible_actions():
			self.observation[self.helper.oc.PA_FIRST:self.helper.oc.PA_LAST+1] = possible_actions

	def update_speed(self, time_stamp, max_speed, ego_speed):
		self.last_observation_update_list[3] = time_stamp
		self.observation[self.helper.oc.MAX_SPEED] = max_speed #+ self.helper.get_variable_speed_limit_offset()
		self.observation[self.helper.oc.ABS_V] = ego_speed
	
	def update_coloring(self, time_stamp, close_vehicles):
		self.last_observation_update_list[4] = time_stamp
		if self.helper.traffic:
			self.close_vehicles = close_vehicles
	
	def update_overtake_state(self, time_stamp, state):
		self.last_observation_update_list[5] = time_stamp
		# if self.helper.traffic:
			# self.observation[self.helper.oc.OVS] = state #TODO this does not work, if vehicles are teleported during training
			
		
	def color_closest_vehicles(self):
		"""color vehicles observed by agent in blue, others in green"""
		try:
			all_vehicles = self.con.vehicle.getIDList()
			for vehicle in all_vehicles:
				if vehicle != self.ego_vehicle_id:
					if vehicle in self.close_vehicles:
						self.con.vehicle.setColor(vehicle, (0, 0, 255))
					else:
						self.con.vehicle.setColor(vehicle, (0, 255, 0))
		except:
			print("coloring failed.")


	def simulation_step(self):
		"""take a simulation step, update some counters"""
		self.con.simulationStep()
		self.sumo_steps_since_last_reset += 1
		self.sumo_step += 1


	def reset(self):
		"""reset the simulation, remove all vehicles, then add them again in the correct order"""
		if self.simulation_started:
			print("------------------------------------------------------------------------------------------------------------------")
			print("resetting \n")
			print("starting episode ", self.episode)
		self.off_road = False
		self.sumo_steps_since_last_reset = 0
		self.current_speed = self.helper.get_depart_speed()
		self.request_observation_now_flag = True
		if self.gym_params.simulation_type == const.THREE_LANE_HIGHWAY_MULTICAR:
			self.helper.filter_deques()
			# self.helper.set_variable_speed_limit_offsets()

		# get restart position of agent from helpers
		restart_edge_ego = self.helper.get_starting_edge_ego()
		restart_lane_ego = self.helper.get_starting_lane_ego()
		print("starting position: ", restart_lane_ego)
		restart_route_ego = self.helper.get_route_ego()
		type_ID = self.helper.get_ID_Type_ego()

		# try to remove ego vehicle
		try:
			self.con.vehicle.remove(self.ego_vehicle_id)
		except:
			pass

		# remove all vehicles 		
		ids = self.con.vehicle.getIDList()
		for id in ids:
			try:
				self.con.vehicle.remove(id)
			except:
				pass
		for id in self.con.polygon.getIDList():
			try:
				self.con.polygon.remove(id)
			except:
				pass
		
		# simulate one step in order to remove vehicles from simulation
		self.simulation_step()

		# add new ego vehicle on random starting position and set parameters
		depart_pos = self.helper.get_starting_position_on_lane()
		depart_speed = self.helper.get_depart_speed()
		self.con.vehicle.add(self.ego_vehicle_id, restart_route_ego, typeID=type_ID, depart=None, departPos=depart_pos,\
							 departSpeed=depart_speed, departLane=restart_lane_ego.split("_")[1])
		self.con.vehicle.setLaneChangeMode(self.ego_vehicle_id, 0)
		self.con.vehicle.setSpeed(self.ego_vehicle_id, 0)
		self.con.vehicle.setSpeedMode(self.ego_vehicle_id, 0)
		self.con.vehicle.setColor(self.ego_vehicle_id, (255,0,0))
		self.con.vehicle.subscribeContext(
            str(self.ego_vehicle_id),
			c.CMD_GET_VEHICLE_VARIABLE,
			self.gym_params.context_range,
            [c.VAR_LANE_INDEX, c.VAR_POSITION3D, c.VAR_ANGLE, c.VAR_SPEED, c.VAR_ACCELERATION, c.VAR_WIDTH,
             c.VAR_LENGTH, c.VAR_VEHICLECLASS, c.VAR_ROAD_ID, c.VAR_LANE_ID])
		self.con.vehicle.subscribe(
            str(self.ego_vehicle_id),
            [c.VAR_ACCELERATION, c.VAR_ANGLE, c.VAR_LENGTH, c.VAR_SPEED, c.POSITION_LON_LAT, c.VAR_POSITION3D])
		self.simulation_step()

		# add previously removed vehicles
		self.helper.add_obstacle_vehicles()
		self.simulation_step()
		self.con.polygon.add("gap_1000", [(10,10), (20,20)], (255,100,0), layer=10)
		

		counter = 0
		# wait for agent vehicle to spawn
		while not self.ego_vehicle_id in self.con.vehicle.getIDList():
			self.simulation_step()
			counter += 1
			if counter > 500:
				raise MissingVehicleError("no ego vehicle in simulation, something wrong with reset")

		# center camera on agent
		if self.con._process.args[0][-3:] == "gui":
			self.con.gui.trackVehicle("View #0", self.ego_vehicle_id)

		return self._observation()


	def render(self, mode='gui', close=False):
		"""this is done by runner on startup"""
		pass
	
	def reward(self):
		return self.helper.get_reward()

	def action_to_string(self, action):
		"""translate action to string"""
		string = ""
		if action == 0:
			string = "right"
		elif action == 1:
			string = "default"
		elif action == 2:
			string = "left"
		elif action == 3:
			string = "accel"
		elif action == 4:
			string = "decel"
		else:
			raise ActionError("this action ({action}) does not exist")
		return string

	def get_legal_actions(self): 
		"""returns 5 element array with bool for each action, based on situation assessment of ontology"""
		legal_actions = self.observation[self.helper.oc.PA_FIRST:self.helper.oc.PA_LAST+1]
		return legal_actions

	def filter_actions(self, action):
		"""replace chosen action with fallback action if chosen action is illegal"""
		#TODO: currently not in use
		legal_actions = self.get_legal_actions()
		if legal_actions[action]:
			return action
		else: # define fallback actions, for now default
			self.impossible_action_taken = True
			if self.enable_debug_prints:
				print(f"impossible Action: {self.action_to_string(action)} -> {self.action_to_string(1)}")
			return 1

	def take_action(self, action): # 0: LC right, 1: default, 2: LC left, 3: accel, 4: decel
		"""execute the sumo commands corresponding to the chosen action"""
		if self.enable_debug_prints:
			print("chosen Action: ", self.action_to_string(action))

		if self.enable_action_filtering:
			action = self.filter_actions(action)
		
		if self.helper.get_observe_possible_actions():
			self.action_was_valid = bool(self.observation[self.helper.oc.PA_FIRST + action])

		accel = 0
		if action <= 2: # left, stay, right
			self.change_Lane(self.ego_vehicle_id, action-1)
		elif action == 3:
			accel = 1
		elif action == 4:
			accel = -1
		
		# New speed
		self.current_speed = self.current_speed + accel
		# Exceeded global speed limit (whats physically possible)
		if self.current_speed > self.gym_params.max_speed :
			self.current_speed = self.gym_params.max_speed 
		elif self.current_speed < 0 :
			self.current_speed = 0

		self.con.vehicle.setSpeed(self.ego_vehicle_id, self.current_speed)
		self.con.vehicle.setSpeedMode(self.ego_vehicle_id, 0)

		

	def change_Lane(self, vehicle_id, direction): 
		"""initiate lane change in given direction (1=left, 0=stay, -1=right), if possible. track offroads"""
		
		# get number of available lanes
		lane_number = self.observation[self.helper.oc.NUM_LANE] 
		# get ego lane index
		lane_index = self.observation[self.helper.oc.IDX] 
		# check if neighboring lane exists
		if (lane_index + direction > lane_number -1) or (lane_index + direction < 0):
			self.off_road = True
		# perform relative lane change
		else:
			self.con.vehicle.changeLaneRelative(vehicle_id, direction, 1)
		self.con.vehicle.setLaneChangeMode(vehicle_id, 0)


	def step(self, action):	  	
		"""step function called by agent class
		Takes action, returns reward and observation
		"""	
		self.chosen_action = action

		if self.enable_debug_prints:
			print("\nStep: ", self.sumo_step)
			print("State:")
			self.helper.print_observations()

		# do action
		if self.ego_vehicle_id in self.con.vehicle.getIDList():
			self.take_action(action)
		else:
			raise MissingVehicleError("no ego vehicle in step function")
		
		# teleport far away vehicles closer to ego
		if self.sumo_step % 100 == 0:
			self.helper.redistribute_vehicles()

		# update safety gap visualization
		if self.helper.traffic:
			self.helper.visualize_safety_gaps()

		self.simulation_step()

		obs = self._observation()
		if self.enable_debug_prints:
			print("New State: ")
			self.helper.print_observations()
		# Get reward and check for terminal state
		r, terminal, info = self.reward()
		
		if self.enable_debug_prints:
			print("Reward: ", r)
			print("Info: ", info)

		return obs, r, terminal, info

	def synchronized(self):
		"""check if all observation topics were reveived"""
		for i in range(1, len(self.last_observation_update_list)):
			if self.last_observation_update_list[0] != self.last_observation_update_list[i]:
				return False
		return int(self.last_observation_request * 1_000_000) == int(self.last_observation_update_list[0])
