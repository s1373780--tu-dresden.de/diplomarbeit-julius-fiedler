from .sumo_env_onto import SUMOEnvOnto
import os
import traci   



class SUMOEnvOnto_Initializer(SUMOEnvOnto):
	def __init__(self, con: traci.connection, gym_params):
		super(SUMOEnvOnto_Initializer, self).__init__(con = con, gym_params=gym_params)
