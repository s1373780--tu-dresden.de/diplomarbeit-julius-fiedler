from gym.envs.registration import register


register(
    id='SumoGUI_Onto-v0',
    entry_point='framework.gym_sumo.environment.envs:SUMOEnvOnto_Initializer',
)