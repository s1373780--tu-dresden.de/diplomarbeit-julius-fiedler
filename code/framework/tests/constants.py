### Constants for selection of Simulation environment

THREE_LANE_CIRCLE = "three_lane_circle"
TESTFELD_DD = "testfeld_dd"
SCENARIO_1 = "scenario_1"
SCENARIO_2 = "scenario_2"
NARROW = "narrow"
THREE_LANE_HIGHWAY = "three_lane_highway"
THREE_LANE_HIGHWAY_MULTICAR = "three_lane_highway_multicar"
THREE_LANE_HIGHWAY_MULTICAR_DETERMINISTIC = "three_lane_highway_multicar_deterministic"